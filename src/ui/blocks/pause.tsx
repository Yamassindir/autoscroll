import { useState } from "react";
import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { verifyEmail } from "@helpers/verify";
import { IPausingRecipeEmail } from "@interfaces/pausing";
import { IRuntimeStyles } from "@hooks/styles";
import { TAutoFocus } from "@hooks/focus";
import { TElement } from "@interfaces/element";
import { Block } from ".";
import { BlockTitle } from "./title";
import { BlockDescription } from "./description";
import { BlockButtons } from "./buttons";
import { EmailFabric } from "@tripetto/runner-fabric/components/email";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { MailIcon } from "@ui/icons/mail";
import { CancelIcon } from "@ui/icons/cancel";

export const BLOCK_PAUSE = "pause";

export const PauseBlock = (props: {
    readonly controller: IPausingRecipeEmail | undefined;
    readonly view: TRunnerViews;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly isPage: boolean;
    readonly onElement: TElement;
    readonly onAutoFocus: TAutoFocus;
}) => {
    const [value, setValue] = useState(props.controller?.emailAddress || "");
    const [hadFocus, setFocus] = useState(false);
    const isValid = verifyEmail(value);
    const isActivated = props.controller ? true : false;

    return (
        <Block
            key={BLOCK_PAUSE}
            element={props.onElement(BLOCK_PAUSE)}
            styles={props.styles}
            view={props.view}
            isPage={props.isPage}
            isActivated={isActivated}
            isHidden={!props.controller}
        >
            <BlockTitle>{props.l10n.pgettext("runner#8|⏸ Pause conversation", "Pause this conversation")}</BlockTitle>
            <BlockDescription>
                {props.l10n.pgettext(
                    "runner#8|⏸ Pause conversation",
                    "Receive a link by email to resume later on any device, right where you left off."
                )}
            </BlockDescription>
            <EmailFabric
                styles={props.styles.inputs}
                required={true}
                disabled={!props.controller || props.controller.isCompleting}
                value={value}
                placeholder={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Your email address...")}
                error={hadFocus && !isValid}
                onChange={setValue}
                onFocus={() => setFocus(true)}
                onAutoFocus={props.onAutoFocus(
                    {
                        key: BLOCK_PAUSE,
                    },
                    isActivated
                )}
                onSubmit={() => isValid && props.controller && props.controller.complete(value)}
                onCancel={() => props.controller && props.controller.cancel()}
            />
            <BlockButtons styles={props.styles} isActivated={true}>
                <ButtonFabric
                    styles={props.styles.buttons}
                    icon={MailIcon}
                    label={
                        props.controller && props.controller.isCompleting
                            ? props.l10n.pgettext("runner#3|🩺 Status information", "Pausing...")
                            : props.l10n.pgettext("runner#8|⏸ Pause conversation", "Receive resume link")
                    }
                    disabled={!props.controller || props.controller.isCompleting || !isValid}
                    onClick={() => props.controller && props.controller.complete(value)}
                />
                <ButtonFabric
                    styles={{ ...props.styles.buttons, mode: "outline" }}
                    icon={CancelIcon}
                    disabled={!props.controller || props.controller.isCompleting}
                    label={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Cancel pausing")}
                    onClick={() => props.controller && props.controller.cancel()}
                />
            </BlockButtons>
        </Block>
    );
};
