import { styled } from "styled-components";

export const BlockDescription = styled.p<{
    $alignment?: "left" | "center" | "right";
}>`
    margin: 0;
    padding: 0;
    font-size: 1em;
    text-align: ${(props) => props.$alignment || "left"};
    user-select: none;
`;
