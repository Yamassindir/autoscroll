import { styled } from "styled-components";
import { ReactNode } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n, castToBoolean } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { BlockPreviousButton } from "@ui/blocks/previous";
import { BannerElement } from "@ui/messages/banner";
import { Banner } from "@ui/messages/banner";
import { MARGIN, MAX_WIDTH, OFFSET, SIZE, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";

export const Blocks = styled.div<{
    $styles: IRuntimeStyles;
    $view?: TRunnerViews;
    $center?: boolean;
    $isPage?: boolean;
    $isMessage?: boolean;
    $isCalculated?: boolean;
    $size?: number;
}>`
    display: ${(props) => (!props.$isMessage && props.$styles.direction === "horizontal" ? "flex" : "block")};
    min-width: 100%;
    flex-direction: ${(props) => !props.$isMessage && props.$styles.direction === "horizontal" && "row"};
    flex-wrap: ${(props) => !props.$isMessage && props.$styles.direction === "horizontal" && "nowrap"};
    align-items: ${(props) => !props.$isMessage && props.$styles.direction === "horizontal" && "start"};
    align-self: ${(props) => props.$center && "center"};
    opacity: ${(props) => (castToBoolean(props.$isCalculated, true) ? 1 : 0)};
    font-size: ${(props) => (props.$size && `${props.$size}px`) || undefined};
    line-height: ${(props) => (props.$size && "1.5em") || undefined};

    > div {
        color: ${(props) => props.$styles.font.color};
        flex: ${(props) => !props.$isMessage && props.$styles.direction === "horizontal" && "0 0 calc(100vw)"};
        transition: color 0.15s ease-in-out;
    }
`;

export const BlockElement = styled.div<{
    $view: TRunnerViews;
    $styles: IRuntimeStyles;
    $isPage: boolean;
    $isActivated?: boolean;
    $isHidden?: boolean;
    $isLocked?: boolean;
    $isMessage?: boolean;
    $hasBanner?: boolean;
}>`
    display: block;
    position: relative;
    contain: content;
    opacity: ${(props) => (props.$isMessage || (props.$isActivated && !props.$isLocked) ? 1 : props.$isHidden ? 0 : 0.2)};
    pointer-events: ${(props) => (props.$isMessage || (props.$isActivated && !props.$isLocked) ? "auto" : "none")};
    transition:
        opacity 0.5s,
        color 0.15s ease-in-out !important;
    user-select: ${(props) => (!props.$isMessage && props.$isActivated ? "auto" : "none")};
    padding-left: ${(props) => `${props.$isPage || props.$view !== "live" || props.$styles.showEnumerators ? MARGIN : OFFSET}px`};
    padding-right: ${(props) => `${props.$isPage || props.$view !== "live" || props.$styles.showScrollbar ? MARGIN : OFFSET}px`};
    padding-top: ${(props) => `${MARGIN + (props.$isPage || props.$view !== "live" ? 8 : 0)}px`};
    padding-bottom: ${(props) => `${MARGIN + (props.$isPage || props.$view !== "live" ? 8 : 0)}px`};
    z-index: 1;

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        padding-left: ${(props) =>
            `${props.$isPage || props.$view !== "live" || props.$styles.showEnumerators ? SMALL_SCREEN_MARGIN : OFFSET}px`};
        padding-right: ${(props) =>
            `${props.$isPage || props.$view !== "live" || props.$styles.showScrollbar ? SMALL_SCREEN_MARGIN : OFFSET}px`};
    }

    &:first-child {
        padding-top: ${(props) => !props.$isPage && props.$view === "live" && `${OFFSET}px`};
    }

    &:last-child {
        padding-bottom: ${(props) =>
            !props.$isPage &&
            props.$view === "live" &&
            !props.$hasBanner &&
            (props.$isMessage || !props.$styles.navigation) &&
            `${OFFSET}px`};
    }

    > div:first-child {
        max-width: ${(props) => props.$styles.direction === "horizontal" && props.$isPage && `${MAX_WIDTH - MARGIN * 2}px`};

        > * {
            margin-top: ${16 / SIZE}em !important;

            &:first-child {
                margin-top: 0 !important;
            }
        }

        > h2 + p {
            margin-top: ${8 / SIZE}em !important;
        }

        > h2 + h3 {
            margin-top: ${8 / SIZE}em !important;
        }

        > h3 + p {
            margin-top: ${8 / SIZE}em !important;
        }
    }

    > div:nth-child(2) {
        max-width: ${(props) => props.$styles.direction === "horizontal" && props.$isPage && `${MAX_WIDTH}px`};
    }

    > div {
        margin-left: ${(props) => props.$styles.direction === "horizontal" && props.$isPage && "auto"};
        margin-right: ${(props) => props.$styles.direction === "horizontal" && props.$isPage && "auto"};
    }
`;

const BlockSurfaceElement = styled.div<{
    $view: TRunnerViews;
    $styles: IRuntimeStyles;
    $isPage: boolean;
    $isActivated?: boolean;
}>`
    position: absolute;
    left: ${(props) => (props.$view === "live" && !props.$isPage ? `${props.$styles.showEnumerators ? MARGIN : OFFSET}px` : 0)};
    top: 0;
    right: 0;
    bottom: 0;
    pointer-events: ${(props) => (props.$isActivated ? "none" : "auto")};
    cursor: default;

    ${BannerElement} {
        position: absolute;
        bottom: ${(props) => (props.$isPage || props.$view !== "live" ? `8px` : 0)};
        left: ${(props) => (props.$isPage || props.$view !== "live" ? `${MARGIN}px` : undefined)};

        > a {
            position: relative;
            top: 0.1em;
        }

        @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
            left: ${(props) => (props.$isPage || props.$view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : undefined)};
        }
    }
`;

export const Block = (props: {
    readonly identifier?: string;
    readonly element?: (ref: HTMLElement | null) => void;
    readonly l10n?: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly bookmark?: string;
    readonly isPage: boolean;
    readonly isActivated?: boolean;
    readonly isHidden?: boolean;
    readonly isLocked?: boolean;
    readonly isMessage?: boolean;
    readonly width?: number;
    readonly top?: number;
    readonly showBanner?: boolean;
    readonly onActivate?: () => void;
    readonly onPrevious?: () => void;
    readonly children?: ReactNode;
}) => (
    <BlockElement
        ref={props.element}
        data-block={props.identifier}
        style={{
            width: props.width,
            top: props.top,
        }}
        $view={props.view}
        $styles={props.styles}
        $isPage={props.isPage}
        $isActivated={props.isActivated}
        $isHidden={props.isHidden}
        $isLocked={props.isLocked}
        $isMessage={props.isMessage}
        $hasBanner={props.l10n && props.onActivate && !props.styles.navigation && !props.styles.noBranding && props.view !== "preview"}
    >
        <div>{props.children}</div>
        {props.l10n && props.onActivate && (
            <BlockSurfaceElement
                onClick={(!props.isActivated && !props.isHidden && props.onActivate) || undefined}
                $view={props.view}
                $styles={props.styles}
                $isPage={props.isPage}
                $isActivated={props.isActivated}
            >
                {props.styles.showPreviousButton && props.onPrevious && (
                    <BlockPreviousButton
                        l10n={props.l10n}
                        styles={props.styles}
                        view={props.view}
                        isPage={props.isPage}
                        isActivated={props.isActivated || false}
                        onClick={props.onPrevious}
                    />
                )}
                {!props.styles.navigation && (
                    <Banner l10n={props.l10n} styles={props.styles} view={props.view} alignment="left" visible={props.showBanner} />
                )}
            </BlockSurfaceElement>
        )}
    </BlockElement>
);
