import { styled } from "styled-components";
import { ReactNode } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { castToBoolean } from "@tripetto/runner";
import { SIZE } from "@ui/const";

const ButtonsElement = styled.nav<{
    $styles: IRuntimeStyles;
    $isActivated: boolean;
    $alignment: "left" | "center" | "right";
}>`
    margin-top: 0 !important;
    min-height: ${45 / SIZE}em;
    display: flex;
    flex-wrap: wrap;
    justify-content: ${(props) => props.$alignment};
    opacity: ${(props) => (props.$isActivated ? 1 : 0)};
    transform: ${(props) =>
        props.$isActivated
            ? "none"
            : props.$styles.direction === "horizontal"
            ? `translateX(-${16 / SIZE}em)`
            : `translateY(${16 / SIZE}em)`};
    transition:
        opacity 0.3s ease-out 0.2s,
        transform 0.3s ease-out 0.2s;

    > * {
        margin-top: ${8 / SIZE}em !important;
        margin-left: ${16 / SIZE}em;

        &:first-child {
            margin-left: 0;
        }
    }

    &:first-child > * {
        margin-top: 0 !important;
    }
`;

export const BlockButtons = (props: {
    readonly styles: IRuntimeStyles;
    readonly isActivated?: boolean;
    readonly alignment?: "left" | "center" | "right";
    readonly children?: ReactNode;
}) => (
    <ButtonsElement $styles={props.styles} $isActivated={castToBoolean(props.isActivated, true)} $alignment={props.alignment || "left"}>
        {props.children}
    </ButtonsElement>
);
