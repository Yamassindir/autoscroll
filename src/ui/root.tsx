import { StyleSheetManager, createGlobalStyle, css, styled } from "styled-components";
import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { DateTime, Num, Str } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { color } from "@tripetto/runner-fabric/color";
import { IViewport } from "@interfaces/viewport";
import { IRuntimeStyles } from "@hooks/styles";
import { TOverlayProvider } from "@tripetto/runner-fabric/overlay";
import { Frame, FrameConfig } from "@hooks/frame";
import { attach, detach } from "@helpers/resizer";
import { INavigation, Navigation, NavigationElement } from "@ui/navigation";
import { MARGIN, MAX_WIDTH, NAVIGATION_HEIGHT, OFFSET, SMALL_SCREEN_SIZE } from "@ui/const";

const RootFrame = styled(Frame).withConfig(FrameConfig)<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
}>`
    background-color: transparent !important;
    border: none !important;
    position: ${(props) => (props.$view === "live" && props.$isPage ? "fixed" : "relative")};
    left: ${(props) =>
        props.$view === "live"
            ? props.$isPage || props.$styles.direction === "horizontal"
                ? 0
                : `-${props.$styles.showEnumerators ? MARGIN : OFFSET}px`
            : undefined};
    top: ${(props) => (props.$view === "live" ? 0 : undefined)};
    width: ${(props) =>
        props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical"
            ? `calc(100% + ${(props.$styles.showEnumerators ? MARGIN : OFFSET) + (props.$styles.showScrollbar ? 0 : OFFSET)}px);`
            : "100%"};
    height: ${(props) => (props.$view === "live" && !props.$isPage ? 0 : "100%")};
    margin-right: ${(props) =>
        (props.$view === "live" &&
            !props.$isPage &&
            props.$styles.direction === "vertical" &&
            `-${(props.$styles.showEnumerators ? MARGIN : OFFSET) + (props.$styles.showScrollbar ? 0 : OFFSET)}px`) ||
        undefined};

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        left: ${(props) =>
            props.$view === "live" ? (props.$isPage || props.$styles.direction === "horizontal" ? 0 : `-${OFFSET}px`) : undefined};
        width: ${(props) =>
            props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical"
                ? `calc(100% + ${props.$styles.showScrollbar ? OFFSET : 0}px);`
                : "100%"};
        margin-right: ${(props) =>
            (props.$view === "live" &&
                !props.$isPage &&
                props.$styles.direction === "vertical" &&
                `-${OFFSET + (props.$styles.showScrollbar ? 0 : OFFSET)}px`) ||
            undefined};
    }
`;

const RootBody = createGlobalStyle<{
    $customCSS?: string;
}>`
    body {
        overflow: hidden;
        background-color: transparent;

        ${(props) =>
            props.$customCSS &&
            css`
                ${Str.replace(props.$customCSS, "tripetto-block-", "@tripetto/block-")}
            `}
    }
`;

const BackgroundElement = styled.div<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
}>`
    position: absolute;
    left: ${(props) =>
        props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical"
            ? `${props.$styles.showEnumerators ? MARGIN : OFFSET}px`
            : 0};
    right: ${(props) =>
        props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical" && !props.$styles.showScrollbar
            ? `${OFFSET}px`
            : 0};
    top: 0;
    bottom: 0;
    background-color: ${(props) => color(props.$styles.background.color)};
    background-image: ${(props) =>
        (props.$styles.background.url &&
            props.$styles.background.opacity > 0 &&
            `${
                (props.$styles.background.opacity < 1 &&
                    `linear-gradient(${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )},${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )}),`) ||
                ""
            }url("${props.$styles.background.url}")`) ||
        undefined};
    background-size: ${(props) =>
        (props.$styles.background.url && props.$styles.background.positioning !== "repeat" && props.$styles.background.positioning) ||
        undefined};
    background-repeat: ${(props) =>
        props.$styles.background.url && props.$styles.background.positioning === "repeat" ? "repeat" : "no-repeat"};
    background-position: center center;
    transition: background 0.15s ease-in-out;
`;

const RootElement = styled.div<{
    $view: TRunnerViews;
    $isPage: boolean;
    $autoHeight: boolean;
    $disableScrolling: boolean;
    $styles: IRuntimeStyles;
    $fontFamily: string;
    $navigation?: INavigation;
}>`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: ${(props) => (props.$navigation && `${NAVIGATION_HEIGHT}px`) || 0};
    font-family: ${(props) => props.$fontFamily};
    font-size: ${(props) => props.$styles.font.size}px;
    font-variant-ligatures: none;
    line-height: 1.5em;
    outline: 0;
    overflow-x: ${(props) => (props.$styles.direction === "horizontal" ? "auto" : "hidden")};
    overflow-y: ${(props) => (props.$autoHeight || props.$disableScrolling ? "hidden" : "auto")};
    touch-action: ${(props) => (props.$styles.disableScrolling ? "none" : "pan-y")};
    scroll-behavior: smooth;
    scrollbar-width: ${(props) => (!props.$styles.showScrollbar && props.$view !== "preview" ? "none" : undefined)};
    -ms-overflow-style: ${(props) => (!props.$styles.showScrollbar && props.$view !== "preview" ? "none" : undefined)};
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    &::-webkit-scrollbar {
        display: ${(props) => (!props.$styles.showScrollbar && props.$view !== "preview" ? "none" : undefined)};
    }

    > div {
        max-width: ${(props) => props.$styles.direction === "vertical" && props.$isPage && `${MAX_WIDTH}px`};
        min-height: ${(props) => (props.$view !== "live" || props.$isPage ? "100%" : undefined)};
        margin-left: ${(props) => props.$styles.direction === "vertical" && props.$isPage && "auto"};
        margin-right: ${(props) => props.$styles.direction === "vertical" && props.$isPage && "auto"};
        display: flex;
    }

    * {
        font-family: ${(props) => props.$fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    ${NavigationElement} {
        left: ${(props) =>
            props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical"
                ? `${props.$styles.showEnumerators ? MARGIN : OFFSET}px`
                : undefined};
        right: ${(props) =>
            props.$view === "live" && !props.$isPage && props.$styles.direction === "vertical" && !props.$styles.showScrollbar
                ? `${OFFSET}px`
                : undefined};
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        font-size: ${(props) => props.$styles.font.sizeSmall}px;
    }
`;

const OverlayElement = styled.div<{
    $view: TRunnerViews;
    $styles: IRuntimeStyles;
    $fontFamily: string;
}>`
    font-family: ${(props) => props.$fontFamily};
    font-size: ${(props) => props.$styles.font.size}px;
    font-variant-ligatures: none;
    line-height: 1.5em;
    outline: 0;

    * {
        font-family: ${(props) => props.$fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        font-size: ${(props) => props.$styles.font.sizeSmall}px;
    }
`;

const Content = (props: { readonly children?: ReactNode; readonly onChange: (height: number, force: boolean) => void }) => {
    const contentRef = useRef<HTMLElement>() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        const ref = attach((force) => {
            if (contentRef.current) {
                props.onChange(contentRef.current.getBoundingClientRect().height, force);
            }
        });

        return () => detach(ref);
    });

    return <div ref={contentRef}>{props.children}</div>;
};

export const AutoscrollRoot = (props: {
    readonly frameRef: MutableRefObject<HTMLIFrameElement>;
    readonly view: TRunnerViews;
    readonly styles: IRuntimeStyles;
    readonly overlayProvider: TOverlayProvider;
    readonly isPage: boolean;
    readonly height?: number | "auto";
    readonly disableScrolling: boolean;
    readonly onResize: () => void;
    readonly onScroll: (rect: IViewport) => void;
    readonly onKey: (action: "next" | "previous") => void;
    readonly onTouch?: () => void;
    readonly navigation?: INavigation;
    readonly title?: string;
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly children?: ReactNode;
}) => {
    const [contentHeight, setContentHeight] = useState(0);
    const rootRef = useRef<HTMLDivElement>() as MutableRefObject<HTMLDivElement | null>;
    const scrollbarSize = useRef(0);
    const scrollRef = useRef<() => void>();
    const scrollSmoothSupport = useRef<boolean>();
    const scrollAnimationRef = useRef<() => void>();
    const onScroll = (scrollRef.current = () => {
        const scrollRect = rootRef.current?.getBoundingClientRect();

        props.onScroll({
            timestamp: DateTime.precise,
            left: scrollRect?.left || 0,
            top: scrollRect?.top || 0,
            width: scrollRect?.width || 0,
            height: scrollRect?.height || 0,
            scrollLeft: rootRef.current?.scrollLeft || 0,
            scrollTop: rootRef.current?.scrollTop || 0,
            scrollTo: (left: number, top: number, smooth: boolean = true) => {
                if (rootRef.current) {
                    top = top > 0 ? Num.round(top) : 0;
                    left = left > 0 ? Num.round(left) : 0;

                    rootRef.current.style.scrollBehavior = smooth ? "" : "auto";

                    if (scrollSmoothSupport.current === false || !rootRef.current.scrollTo) {
                        if (scrollAnimationRef.current) {
                            scrollAnimationRef.current();
                        }

                        if (smooth) {
                            const element = rootRef.current;
                            const start = DateTime.precise;
                            const offsetX = ~~element.scrollLeft;
                            const offsetY = ~~element.scrollTop;
                            const deltaX = ~~(left - offsetX);
                            const deltaY = ~~(top - offsetY);
                            const duration = 400;
                            const animate = () => {
                                const requestId = requestAnimationFrame(() => {
                                    const t = Num.range(DateTime.precise - start, 0, duration);
                                    const f = t / duration;
                                    const x = -deltaX * f * (f - 2);
                                    const y = -deltaY * f * (f - 2);

                                    element.scrollLeft = offsetX + x;
                                    element.scrollTop = offsetY + y;

                                    if (t !== duration) {
                                        animate();
                                    } else {
                                        element.scrollLeft = left;
                                        element.scrollTop = top;
                                    }
                                });

                                scrollAnimationRef.current = () => cancelAnimationFrame(requestId);
                            };

                            animate();
                        } else {
                            rootRef.current.scrollLeft = left;
                            rootRef.current.scrollTop = top;
                        }
                    } else {
                        rootRef.current.scrollTo({
                            left,
                            top,
                            behavior: smooth ? "smooth" : "auto",
                        });
                    }
                }
            },
        });
    });

    useEffect(() => {
        if (props.frameRef.current && props.view === "live" && (props.isPage || props.styles.autoFocus)) {
            props.frameRef.current.focus();
        }
    }, [contentHeight]);

    useEffect(() => {
        if (typeof scrollSmoothSupport.current !== "boolean" && rootRef.current) {
            scrollSmoothSupport.current = getComputedStyle(rootRef.current).scrollBehavior === "smooth";
        }

        if (
            !scrollbarSize.current &&
            props.styles.direction === "horizontal" &&
            (props.styles.showScrollbar || props.view === "preview") &&
            rootRef.current &&
            rootRef.current.clientHeight > 0 &&
            rootRef.current.offsetHeight > rootRef.current.clientHeight
        ) {
            scrollbarSize.current = Num.max(rootRef.current.offsetHeight - rootRef.current.clientHeight, 0);

            setContentHeight(contentHeight + scrollbarSize.current);
        }

        onScroll();

        if (rootRef.current && props.styles.disableScrolling) {
            const wheelElement = rootRef.current;
            const wheelListener = (wheelEvent: WheelEvent) => {
                wheelEvent.preventDefault();
                wheelEvent.stopPropagation();

                return false;
            };

            wheelElement.addEventListener("wheel", wheelListener, {
                passive: false,
            });

            return () => {
                wheelElement.removeEventListener("wheel", wheelListener);
            };
        }

        return;
    });

    return (
        <RootFrame
            title={props.title}
            style={{
                ...props.customStyle,
                height:
                    props.customStyle && props.customStyle.height && props.customStyle.height !== "auto"
                        ? props.customStyle.height
                        : props.height === "auto"
                        ? contentHeight + (props.navigation ? NAVIGATION_HEIGHT : 0)
                        : props.height,
            }}
            className={props.className}
            onTouch={props.onTouch}
            frameRef={props.frameRef}
            resizeRef={scrollRef}
            font={props.styles.font && props.styles.font.family}
            $view={props.view}
            $isPage={props.isPage}
            $styles={props.styles}
        >
            {(doc: Document, fontFamily: string) => (
                <StyleSheetManager target={doc.head}>
                    <>
                        <RootBody $customCSS={props.customCSS} />
                        <BackgroundElement $view={props.view} $isPage={props.isPage} $styles={props.styles} />
                        <RootElement
                            ref={rootRef}
                            tabIndex={0}
                            onScroll={onScroll}
                            onKeyDown={(e: React.KeyboardEvent<HTMLDivElement>) => {
                                if (e.key === "PageUp") {
                                    e.preventDefault();

                                    props.onKey("previous");
                                }

                                if (e.key === "PageDown") {
                                    e.preventDefault();

                                    props.onKey("next");
                                }

                                if (doc.activeElement?.tagName === "INPUT" || doc.activeElement?.tagName === "TEXTAREA") {
                                    return;
                                }

                                if (
                                    (e.key === "ArrowUp" && props.styles.direction === "vertical") ||
                                    (e.key === "ArrowLeft" && props.styles.direction === "horizontal")
                                ) {
                                    e.preventDefault();

                                    props.onKey("previous");
                                } else if (
                                    (e.key === "ArrowDown" && props.styles.direction === "vertical") ||
                                    (e.key === "ArrowRight" && props.styles.direction === "horizontal")
                                ) {
                                    e.preventDefault();

                                    props.onKey("next");
                                } else if (e.key === " " && doc.activeElement?.tagName !== "BUTTON") {
                                    e.preventDefault();

                                    if (e.shiftKey) {
                                        props.onKey("previous");
                                    } else {
                                        props.onKey("next");
                                    }
                                }
                            }}
                            $view={props.view}
                            $isPage={props.isPage}
                            $autoHeight={props.height === "auto"}
                            $disableScrolling={props.disableScrolling}
                            $styles={props.styles}
                            $fontFamily={fontFamily}
                            $navigation={props.navigation}
                        >
                            <Content
                                onChange={(height, force) => {
                                    height =
                                        Num.ceil(height) +
                                        ((props.styles.direction === "horizontal" &&
                                            (props.styles.showScrollbar || props.view === "preview") &&
                                            scrollbarSize.current) ||
                                            0);

                                    if (height !== contentHeight || force) {
                                        setContentHeight(height);

                                        props.onResize();
                                    }
                                }}
                            >
                                {props.children}
                            </Content>
                        </RootElement>
                        {props.navigation && <Navigation {...props.navigation} isPage={props.isPage} fontFamily={fontFamily} />}
                        <props.overlayProvider
                            element={OverlayElement}
                            props={{
                                $styles: props.styles,
                                $view: props.view,
                                $fontFamily: fontFamily,
                            }}
                        />
                    </>
                </StyleSheetManager>
            )}
        </RootFrame>
    );
};
