import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { color } from "@tripetto/runner-fabric/color";
import { CancelIcon } from "@ui/icons/cancel";
import { SIZE } from "@ui/const";

const Buttons = styled.div`
    display: block;
    margin-top: ${16 / SIZE}em;

    * + * {
        margin-left: ${16 / SIZE}em;
    }
`;

export const RejectedError = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly onDiscard: () => void;
}) => (
    <>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Your data is rejected.")}</b>
        <br />
        {props.l10n.pgettext(
            "runner#9|⚠ Errors|Rejected error",
            "Unfortunately, your data is marked as invalid and therefore rejected. If you believe this is a mistake, please contact the form owner. We're sorry for the inconvenience."
        )}
        <Buttons>
            <ButtonFabric
                styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                label={props.l10n.pgettext("runner#1|🆗 Buttons", "Discard")}
                icon={CancelIcon}
                onClick={props.onDiscard}
            />
        </Buttons>
    </>
);
