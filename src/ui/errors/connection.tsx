import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { color } from "@tripetto/runner-fabric/color";
import { RetryIcon } from "@ui/icons/retry";
import { CancelIcon } from "@ui/icons/cancel";
import { SIZE } from "@ui/const";

const Buttons = styled.div`
    display: block;
    margin-top: ${16 / SIZE}em;

    * + * {
        margin-left: ${16 / SIZE}em;
    }
`;

export const ConnectionError = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly onRetry: () => void;
    readonly onDiscard: () => void;
}) => (
    <>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Something went wrong while submitting your conversation.")}</b>
        <br />
        {props.l10n.pgettext(
            "runner#9|⚠ Errors|Connection error",
            "Please check your connection and try again (for the techies: The error console of your browser might contain more technical information about what went wrong)."
        )}
        <Buttons>
            <ButtonFabric
                styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                label={props.l10n.pgettext("runner#1|🆗 Buttons", "Retry")}
                icon={RetryIcon}
                onClick={props.onRetry}
            />
            <ButtonFabric
                styles={{ baseColor: color(props.styles.inputs.errorColor, (o) => o.makeBlackOrWhite()), mode: "outline" }}
                label={props.l10n.pgettext("runner#1|🆗 Buttons", "Discard")}
                icon={CancelIcon}
                onClick={props.onDiscard}
            />
        </Buttons>
    </>
);
