import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { color } from "@tripetto/runner-fabric/color";
import { L10n } from "@tripetto/runner";

const ErrorMessageElement = styled.div<{
    $styles: IRuntimeStyles;
    $visible: boolean;
}>`
    position: fixed;
    left: 32px;
    right: 32px;
    top: 32px;
    display: flex;
    justify-content: center;
    opacity: ${(props) => (props.$visible ? 1 : 0)};
    pointer-events: ${(props) => (props.$visible ? "auto" : "none")};
    transform: ${(props) => (props.$visible ? "translateY(0)" : "translateY(-100px)")};
    transition:
        opacity 0.3s ease-out,
        transform 0.3s ease-out;
    z-index: 2;

    > div {
        box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.25);
        cursor: pointer;
        background-color: ${(props) => color(props.$styles.inputs.errorColor, (o) => o.manipulate((m) => m.alpha(0.85)))};
        color: ${(props) => color(props.$styles.inputs.errorColor, (o) => o.makeBlackOrWhite())};
        transition:
            background-color 0.15s ease-in-out,
            color 0.15s ease-in-out;
        padding: 0.7em;
        border-radius: 0.5em;
        user-select: none;
    }
`;

export const ErrorMessage = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly failed: number;
    readonly error?: JSX.Element;
    readonly onClick?: () => void;
}) => (
    <ErrorMessageElement onClick={props.onClick} $styles={props.styles} $visible={props.error || props.failed > 0 ? true : false}>
        <div>
            {props.error ||
                props.l10n.npgettext(
                    "runner#9|⚠ Errors|Input error",
                    "%1 item requires your attention! Click to fix.",
                    "%1 items require your attention! Click to fix.",
                    props.failed || 1
                )}
        </div>
    </ErrorMessageElement>
);
