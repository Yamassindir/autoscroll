import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";

export const EvaluatingElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    display: block;
    color: ${(props) => props.$styles.font.color};
    transition: color 0.15s ease-in-out;
    align-self: center;
    font-size: 0.85em;
    user-select: none;
`;

export const EvaluatingMessage = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) => (
    <EvaluatingElement $styles={props.styles}>
        {props.l10n.pgettext("runner#3|🩺 Status information", "⏳ One moment please...")}
    </EvaluatingElement>
);
