import { IEpilogue, L10n, isBoolean, markdownifyToString, markdownifyToURL } from "@tripetto/runner";
import { TRunnerViews, markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { Block, Blocks } from "@ui/blocks";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockCaption } from "@ui/blocks/caption";
import { BlockDescription } from "@ui/blocks/description";
import { BlockVideo } from "@ui/blocks/video";
import { BlockButtons } from "@ui/blocks/buttons";
import { RestartIcon } from "@ui/icons/restart";
import { EditIcon } from "@ui/icons/edit";

export const Epilogue = (
    props: IEpilogue & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly isPage: boolean;
        readonly repeat?: () => void;
        readonly edit?: () => void;
    }
) => {
    if (props.view === "live" && props.getRedirect && props.getRedirect()) {
        return <></>;
    }

    return (
        <Blocks $styles={props.styles} $view={props.view} $isPage={props.isPage} $center={true} $isMessage={true}>
            <Block styles={props.styles} view={props.view} isPage={props.isPage} isMessage={true}>
                {props.view !== "live" && props.redirectUrl ? (
                    <>
                        <BlockTitle onClick={props.edit} $alignment="center">
                            {props.view === "preview"
                                ? props.l10n.pgettext("runner:autoscroll", "🌍 Redirect preview")
                                : props.l10n.pgettext("runner:autoscroll", "🎉 Test completed")}
                        </BlockTitle>
                        <BlockDescription onClick={props.edit} $alignment="center">
                            {props.l10n.pgettext("runner:autoscroll", "In a live environment the form will redirect to:")}
                            <br />
                            <a href={markdownifyToURL(props.redirectUrl || "", props.context) || "#"} rel="noopener" target="_blank">
                                {markdownifyToURL(props.redirectUrl || "", props.context) ||
                                    props.l10n.pgettext("runner:autoscroll", "The supplied URL is invalid!")}
                            </a>
                        </BlockDescription>
                        {props.view === "test" && (
                            <BlockButtons styles={props.styles} alignment="center">
                                <ButtonFabric
                                    styles={{ ...props.styles.buttons, mode: "outline" }}
                                    label={props.l10n.pgettext("runner:autoscroll", "Test again")}
                                    icon={RestartIcon}
                                    disabled={!props.repeat}
                                    onClick={props.repeat}
                                />
                            </BlockButtons>
                        )}
                    </>
                ) : (
                    <>
                        {props.image && (
                            <BlockImage
                                src={markdownifyToURL(props.image, props.context, undefined, [
                                    "image/jpeg",
                                    "image/png",
                                    "image/svg",
                                    "image/gif",
                                ])}
                                isPage={props.isPage}
                                alignment="center"
                                onClick={props.edit}
                            />
                        )}
                        <BlockTitle onClick={props.edit} $alignment="center">
                            {markdownifyToJSX(
                                props.title ||
                                    (props.view === "test" && props.l10n.pgettext("runner:autoscroll", "🎉 Test completed")) ||
                                    props.l10n.pgettext("runner#2|💬 Messages|Conversation ended", "🎉 It's a wrap!"),
                                props.context
                            )}
                        </BlockTitle>
                        {props.description ? (
                            <BlockDescription onClick={props.edit} $alignment="center">
                                {markdownifyToJSX(props.description, props.context)}
                            </BlockDescription>
                        ) : (
                            !props.title &&
                            props.view === "test" && (
                                <BlockDescription onClick={props.edit} $alignment="center">
                                    {props.l10n.pgettext("runner:autoscroll", "Did you know you can customize this closing message?")}
                                </BlockDescription>
                            )
                        )}
                        {props.video && <BlockVideo src={markdownifyToURL(props.video, props.context)} view={props.view} play={true} />}
                        {(props.button || props.repeatable || props.view === "test") && (
                            <BlockButtons styles={props.styles} alignment="center">
                                {props.button && (
                                    <ButtonFabric
                                        styles={{ ...props.styles.buttons, mode: "fill" }}
                                        label={markdownifyToString(props.button.label, props.context)}
                                        hyperlink={{
                                            url: markdownifyToURL(props.button.url, props.context),
                                            target: props.view === "preview" || props.view === "test" ? "blank" : props.button.target,
                                        }}
                                    />
                                )}
                                {(props.repeatable || props.view === "test") && (
                                    <>
                                        <ButtonFabric
                                            styles={{ ...props.styles.buttons, mode: "outline" }}
                                            label={
                                                props.repeatable
                                                    ? props.l10n.pgettext("runner#1|🆗 Buttons", "Start again")
                                                    : props.l10n.pgettext("runner:autoscroll", "Test again")
                                            }
                                            icon={RestartIcon}
                                            disabled={!props.repeat}
                                            onClick={props.repeat}
                                        />
                                        {props.edit &&
                                            !props.image &&
                                            !props.title &&
                                            !props.description &&
                                            !props.video &&
                                            !props.button &&
                                            !isBoolean(props.repeatable) && (
                                                <ButtonFabric
                                                    styles={{ ...props.styles.buttons, mode: "outline" }}
                                                    label={props.l10n.pgettext("runner:autoscroll", "Customize")}
                                                    icon={EditIcon}
                                                    onClick={props.edit}
                                                />
                                            )}
                                    </>
                                )}
                            </BlockButtons>
                        )}
                    </>
                )}
            </Block>
            {props.view === "live" && !props.styles.noBranding && (
                <Block styles={props.styles} view={props.view} isPage={props.isPage} isMessage={true}>
                    <BlockCaption $alignment="center">
                        {props.l10n.pgettext("runner:autoscroll", "Want to make a form like this for free?")}
                    </BlockCaption>
                    <BlockDescription $alignment="center">
                        {props.l10n.pgettext(
                            "runner:autoscroll",
                            "Tripetto is for making elegantly personal form and survey experiences with response boosting conversational powers."
                        )}
                    </BlockDescription>
                    <BlockButtons styles={props.styles} alignment="center">
                        <ButtonFabric
                            styles={{ ...props.styles.buttons, mode: "fill" }}
                            label={props.l10n.pgettext("runner:autoscroll", "Create one now!")}
                            hyperlink={{
                                url: "https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_autoscroll&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=closing",
                                target: "blank",
                            }}
                        />
                    </BlockButtons>
                </Block>
            )}
        </Blocks>
    );
};
