import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { color } from "@tripetto/runner-fabric/color";

export const PreviewMessageElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    display: block;
    color: ${(props) => color(props.$styles.font.color, (o) => o.manipulate((m) => m.alpha(0.4)))};
    transition: color 0.15s ease-in-out;
    align-self: center;
    font-size: 0.85em;
    user-select: none;
`;

export const PreviewMessage = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) => (
    <PreviewMessageElement $styles={props.styles}>
        {props.l10n.pgettext("runner:autoscroll", "Disabled in preview mode")}
    </PreviewMessageElement>
);
