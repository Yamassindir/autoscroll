import { styled } from "styled-components";
import { color } from "@tripetto/runner-fabric/color";

const ProgressbarElement = styled.div<{
    $color: string;
}>`
    background-color: ${(props) => color(props.$color, (o) => o.manipulate((m) => m.alpha(0.1)))};
    transition: background-color 0.15s ease-in-out;
    border-radius: 0.5em;
    width: 100%;
    height: 8px;
    border-radius: 4px;
    margin-top: 8px;
    margin-bottom: 8px;
`;

const ProgressElement = styled.div<{
    $color: string;
    $percentage: number;
}>`
    background-color: ${(props) => props.$color};
    transition:
        width 0.3s ease,
        background-color 0.15s ease-in-out;
    width: ${(props) => (props.$percentage > 0 ? props.$percentage + "%" : "0")};
    height: 8px;
    border-radius: 4px;
`;

export const Progressbar = (props: { readonly total: number; readonly progress: number; readonly color: string }) => {
    const percentage = props.total > 0 ? (100 / props.total) * props.progress : 0;

    return (
        <ProgressbarElement $color={props.color}>
            <ProgressElement
                role="progressbar"
                $percentage={percentage}
                $color={props.color}
                aria-valuenow={percentage}
                aria-valuemin={0}
                aria-valuemax={100}
            />
        </ProgressbarElement>
    );
};
