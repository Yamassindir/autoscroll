import { TStylesContract } from "@tripetto/runner";

declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

export default (pgettext: (context: string, id: string, ...args: string[]) => string): TStylesContract => ({
    contract: {
        name: PACKAGE_NAME,
        version: PACKAGE_VERSION,
    },
    styles: [
        {
            elements: [
                {
                    type: "color",
                    name: "color",
                    label: pgettext("runner:autoscroll", "Color"),
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Font"),
            optional: true,
            name: "font",
            elements: [
                {
                    type: "static",
                    label: pgettext(
                        "runner:autoscroll",
                        "Use any font available on [Google Fonts](https://fonts.google.com/) or supply a custom font URL. Please note that the Google Fonts name is case sensitive."
                    ),
                },
                {
                    type: "dropdown",
                    name: "family",
                    label: pgettext("runner:autoscroll", "Font family"),
                    options: [
                        {
                            optGroup: pgettext("runner:autoscroll", "Standard fonts"),
                        },
                        {
                            label: pgettext("runner:autoscroll", "Default"),
                            value: "",
                        },
                        {
                            label: "Arial",
                            value: "Arial",
                        },
                        {
                            label: "Arial Black",
                            value: "Arial Black",
                        },
                        {
                            label: "Comic Sans MS",
                            value: "Comic Sans MS",
                        },
                        {
                            label: "Courier New",
                            value: "Courier New",
                        },
                        {
                            label: "Georgia",
                            value: "Georgia",
                        },
                        {
                            label: "Garamond",
                            value: "Garamond",
                        },
                        {
                            label: "Helvetica",
                            value: "Helvetica",
                        },
                        {
                            label: "Impact",
                            value: "Impact",
                        },
                        {
                            label: "Palatino",
                            value: "Palatino",
                        },
                        {
                            label: "Sans-Serif",
                            value: "Sans-Serif",
                        },
                        {
                            label: "Times New Roman",
                            value: "Times New Roman",
                        },
                        {
                            label: "Trebuchet MS",
                            value: "Trebuchet MS",
                        },
                        {
                            label: "Verdana",
                            value: "Verdana",
                        },
                        {
                            optGroup: pgettext("runner:autoscroll", "Custom fonts"),
                        },
                        {
                            custom: pgettext("runner:autoscroll", "Google Fonts or URL"),
                        },
                    ],
                },
                {
                    type: "number",
                    name: "size",
                    label: pgettext("runner:autoscroll", "Font size"),
                    default: 16,
                    min: 8,
                    max: 30,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "number",
                    name: "sizeSmall",
                    label: pgettext("runner:autoscroll", "Font size for small screens"),
                    default: 14,
                    min: 8,
                    max: 30,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "static",
                    label: pgettext(
                        "runner:autoscroll",
                        "_Note_: This size applies to small screens (like mobile devices). Switch to the mobile device preview to see and test this small font size."
                    ),
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Background"),
            optional: true,
            name: "background",
            elements: [
                {
                    type: "color",
                    name: "color",
                    label: pgettext("runner:autoscroll", "Color"),
                },
                {
                    type: "image",
                    name: "url",
                    label: pgettext("runner:autoscroll", "Image (URL)"),
                },
                {
                    type: "number",
                    name: "opacity",
                    label: pgettext("runner:autoscroll", "Opacity"),
                    min: 0,
                    max: 100,
                    suffix: "%",
                    default: 100,
                    dependency: {
                        action: "enable",
                        observe: [
                            {
                                property: "background.color",
                                defined: true,
                            },
                            {
                                property: "background.url",
                                defined: true,
                            },
                        ],
                    },
                },
                {
                    type: "dropdown",
                    name: "positioning",
                    label: pgettext("runner:autoscroll", "Positioning"),
                    options: [
                        {
                            label: pgettext("runner:autoscroll", "Center"),
                            value: "auto",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Repeat"),
                            value: "repeat",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Cover (full screen)"),
                            value: "cover",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Contain (full image stretched over screen)"),
                            value: "contain",
                        },
                        {
                            label: pgettext("runner:autoscroll", "100% width, height centered"),
                            value: "100% auto",
                        },
                        {
                            label: pgettext("runner:autoscroll", "100% height, width centered"),
                            value: "auto 100%",
                        },
                    ],
                    default: "auto",
                    dependency: {
                        action: "disable",
                        observe: [
                            {
                                property: "background.url",
                                value: "",
                            },
                        ],
                    },
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Inputs"),
            optional: true,
            name: "inputs",
            elements: [
                {
                    type: "color",
                    name: "backgroundColor",
                    label: pgettext("runner:autoscroll", "Background color"),
                },
                {
                    type: "color",
                    name: "borderColor",
                    label: pgettext("runner:autoscroll", "Border color"),
                },
                {
                    type: "number",
                    name: "borderSize",
                    label: pgettext("runner:autoscroll", "Border size"),
                    default: 1,
                    min: 1,
                    max: 20,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "number",
                    name: "roundness",
                    label: pgettext("runner:autoscroll", "Roundness"),
                    default: 8,
                    min: 0,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:autoscroll", "Text color"),
                },
                {
                    type: "color",
                    name: "errorColor",
                    label: pgettext("runner:autoscroll", "Required/error color"),
                    default: "red",
                },
                {
                    type: "color",
                    name: "selectionColor",
                    label: pgettext("runner:autoscroll", "Selection color (input buttons)"),
                },
                {
                    type: "color",
                    name: "agreeColor",
                    label: pgettext("runner:autoscroll", "Agree color (input buttons)"),
                    default: "green",
                },
                {
                    type: "color",
                    name: "declineColor",
                    label: pgettext("runner:autoscroll", "Decline color (input buttons)"),
                    default: "red",
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Buttons"),
            optional: true,
            name: "buttons",
            elements: [
                {
                    type: "color",
                    name: "baseColor",
                    label: pgettext("runner:autoscroll", "Button color"),
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:autoscroll", "Text color"),
                },
                {
                    type: "dropdown",
                    name: "mode",
                    label: pgettext("runner:autoscroll", "Style"),
                    options: [
                        {
                            label: pgettext("runner:autoscroll", "Fill"),
                            value: "fill",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Outline"),
                            value: "outline",
                        },
                    ],
                },
                {
                    type: "number",
                    name: "roundness",
                    label: pgettext("runner:autoscroll", "Roundness"),
                    default: 8,
                    min: 0,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "color",
                    name: "finishColor",
                    label: pgettext("runner:autoscroll", "Submit button color"),
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Navigation bar"),
            optional: true,
            name: "navigation",
            elements: [
                {
                    type: "color",
                    name: "backgroundColor",
                    label: pgettext("runner:autoscroll", "Background color"),
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:autoscroll", "Text color"),
                },
                {
                    type: "color",
                    name: "progressbarColor",
                    label: pgettext("runner:autoscroll", "Progressbar color"),
                    dependency: {
                        observe: {
                            property: "showProgressbar",
                            value: true,
                        },
                        action: "enable",
                    },
                },
            ],
            dependency: {
                observe: {
                    property: "showNavigation",
                    value: ["auto", "always"],
                },
                action: "enable",
            },
        },
        {
            title: pgettext("runner:autoscroll", "Appearance"),
            additional: true,
            elements: [
                {
                    type: "radiobuttons",
                    name: "direction",
                    label: pgettext("runner:autoscroll", "Scroll direction"),
                    buttons: [
                        {
                            label: pgettext("runner:autoscroll", "Vertical"),
                            value: "vertical",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Horizontal"),
                            value: "horizontal",
                        },
                    ],
                    default: "vertical",
                },
                {
                    type: "radiobuttons",
                    name: "verticalAlignment",
                    label: pgettext("runner:autoscroll", "Vertical alignment"),
                    buttons: [
                        {
                            label: pgettext("runner:autoscroll", "Top"),
                            value: "top",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Middle"),
                            value: "middle",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Bottom"),
                            value: "bottom",
                        },
                    ],
                    default: "middle",
                },
                {
                    type: "static",
                    label: pgettext("runner:autoscroll", "Visibility"),
                },
                {
                    type: "checkbox",
                    name: "hidePast",
                    label: pgettext("runner:autoscroll", "Hide past blocks"),
                    default: false,
                    dependency: {
                        observe: {
                            property: "direction",
                            value: "horizontal",
                        },
                        action: "disable",
                    },
                },
                {
                    type: "checkbox",
                    name: "hideUpcoming",
                    label: pgettext("runner:autoscroll", "Hide upcoming blocks"),
                    default: false,
                    dependency: {
                        observe: {
                            property: "direction",
                            value: "horizontal",
                        },
                        action: "disable",
                    },
                },
            ],
        },
        {
            title: pgettext("runner:autoscroll", "Options"),
            additional: true,
            elements: [
                {
                    type: "dropdown",
                    name: "showNavigation",
                    label: pgettext("runner:autoscroll", "Show navigation bar"),
                    width: "full",
                    options: [
                        {
                            label: pgettext("runner:autoscroll", "Only when displayed in full page"),
                            value: "auto",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Always"),
                            value: "always",
                        },
                        {
                            label: pgettext("runner:autoscroll", "Never"),
                            value: "never",
                        },
                    ],
                    default: "auto",
                },
                {
                    type: "checkbox",
                    name: "showProgressbar",
                    label: pgettext("runner:autoscroll", "Display progressbar"),
                    default: true,
                    dependency: {
                        observe: {
                            property: "showNavigation",
                            value: ["auto", "always"],
                        },
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "showEnumerators",
                    label: pgettext("runner:autoscroll", "Display numbering"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "autoFocus",
                    label: pgettext("runner:autoscroll", "Automatically gain focus"),
                    description: pgettext("runner:autoscroll", "Only applies when embedding the form."),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showSeparateSubmit",
                    label: pgettext("runner:autoscroll", "Display separate submit button"),
                    default: true,
                },
                {
                    type: "checkbox",
                    name: "showPreviousButton",
                    label: pgettext("runner:autoscroll", "Display back button"),
                    default: true,
                },
                {
                    type: "checkbox",
                    name: "hideRequiredIndicator",
                    label: pgettext("runner:autoscroll", "Hide asterisk for required questions"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showScrollbar",
                    label: pgettext("runner:autoscroll", "Show scrollbar"),
                    default: false,
                    dependency: {
                        observe: [
                            {
                                property: "direction",
                                value: "vertical",
                            },
                            {
                                property: "disableScrolling",
                                value: false,
                                defined: false,
                            },
                        ],
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "disableScrolling",
                    label: pgettext("runner:autoscroll", "Disable scrolling"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "noBranding",
                    label: pgettext("runner:autoscroll", "Hide all the Tripetto branding"),
                    default: false,
                    tier: "licensed",
                },
            ],
        },
    ],
});
