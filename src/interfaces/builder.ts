import { IDefinition, Hooks } from "@tripetto/runner";

export interface IBuilderInstance extends Hooks<"OnChange" | "OnEdit"> {
    definition: IDefinition | undefined;
    edit(sType: "branch" | "section" | "node" | "condition", sId: string): void;
    edit(sType: "epilogue", sId?: string): void;
    edit(sType?: "properties" | "prologue"): void;
}
