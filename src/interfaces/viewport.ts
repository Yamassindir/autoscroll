export interface IViewport {
    readonly timestamp: number;
    readonly left: number;
    readonly top: number;
    readonly width: number;
    readonly height: number;
    readonly scrollLeft: number;
    readonly scrollTop: number;
    readonly scrollTo?: (left: number, top: number, smooth?: boolean) => void;
}
