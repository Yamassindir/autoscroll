import { NgModule } from "@angular/core";
import { TripettoAutoscrollComponent } from "./runner.component";

@NgModule({
    declarations: [TripettoAutoscrollComponent],
    imports: [],
    exports: [TripettoAutoscrollComponent],
})
export class TripettoAutoscrollModule {}
