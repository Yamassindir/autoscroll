import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output } from "@angular/core";
import { IDefinition, IHookPayload, ISnapshot, Instance, L10n, TL10n, isPromise } from "@tripetto/runner";
import {
    IAutoscrollRunner,
    IAutoscrollSnapshot,
    IAutoscrollStyles,
    IBuilderInstance,
    TAutoscrollDisplay,
    TAutoscrollPause,
    run,
} from "@tripetto/runner-autoscroll";
import { IRunnerAttachments, TRunnerPreviewData, TRunnerViews } from "@tripetto/runner-react-hook";
import { CSSProperties } from "react";

@Component({
    selector: "tripetto-runner-autoscroll",
    template: "",
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TripettoAutoscrollComponent implements OnInit, OnDestroy {
    private runnerController?: IAutoscrollRunner;
    private builderController?: IBuilderInstance;
    private initialDefinition?: IDefinition | Promise<IDefinition | undefined>;
    private initialView?: TRunnerViews | undefined;
    private initialStyles?: IAutoscrollStyles | Promise<IAutoscrollStyles | undefined>;
    private initialL10n?: TL10n | Promise<TL10n | undefined>;

    /** Retrieves a reference to the runner instance. */
    get controller(): IAutoscrollRunner | undefined {
        return this.runnerController;
    }

    /** Retrieves the running definition. */
    get definition(): IDefinition | Promise<IDefinition | undefined> | undefined {
        return this.runnerController?.definition || this.initialDefinition;
    }

    /** Specifies the definition to run. */
    @Input() set definition(definition: IDefinition | Promise<IDefinition | undefined> | undefined) {
        if (this.runnerController && definition) {
            this.zone.runOutsideAngular(() => {
                if (isPromise(definition)) {
                    definition.then((value) => {
                        if (value) {
                            this.runnerController!.definition = value;
                        }
                    });
                } else if (definition) {
                    this.runnerController!.l10n = definition;
                }
            });

            return;
        }

        this.initialDefinition = definition;
    }

    /** Retrieves the view mode of the runner. */
    get view(): TRunnerViews {
        return this.runnerController?.view || this.initialView || "live";
    }

    /** Specifies the view mode of the runner. */
    @Input() set view(view: TRunnerViews) {
        if (this.runnerController) {
            this.zone.runOutsideAngular(() => {
                this.runnerController!.view = view;
            });

            return;
        }

        this.initialView = view;
    }

    /** Retrieves the styles (colors, font, size, etc.) for the runner. */
    get styles(): IAutoscrollStyles | Promise<IAutoscrollStyles | undefined> | undefined {
        return this.runnerController?.styles || this.initialStyles;
    }

    /** Specifies the styles (colors, font, size, etc.) for the runner. */
    @Input() set styles(styles: IAutoscrollStyles | Promise<IAutoscrollStyles | undefined> | undefined) {
        if (this.runnerController) {
            this.zone.runOutsideAngular(() => {
                if (isPromise(styles)) {
                    styles.then((value) => {
                        if (value) {
                            this.runnerController!.styles = value;
                        }
                    });
                } else if (styles) {
                    this.runnerController!.styles = styles;
                }
            });

            return;
        }

        this.initialStyles = styles;
    }

    /** Retrieves the localization (locale and translation) information. */
    get l10n(): TL10n | Promise<TL10n | undefined> | undefined {
        return this.runnerController?.l10n || this.initialL10n;
    }

    /** Specifies the localization (locale and translation) information. */
    @Input() set l10n(l10n: TL10n | Promise<TL10n | undefined> | undefined) {
        if (this.runnerController) {
            this.zone.runOutsideAngular(() => {
                if (isPromise(l10n)) {
                    l10n.then((value) => {
                        if (value) {
                            this.runnerController!.l10n = value;
                        }
                    });
                } else if (l10n) {
                    this.runnerController!.l10n = l10n;
                }
            });

            return;
        }

        this.initialL10n = l10n;
    }

    /** Specifies the display mode of the runner. */
    @Input() display?: TAutoscrollDisplay;

    /** Specifies the snapshot that should be restored. */
    @Input() snapshot?: ISnapshot<IAutoscrollSnapshot> | Promise<ISnapshot<IAutoscrollSnapshot> | undefined>;

    /** Try to store sessions in the local store to preserve persistency on navigation between multiple pages that host the runner. */
    @Input() persistent?: boolean;

    /** Specifies a license code for the runner. */
    @Input() license?: string | Promise<string | undefined>;

    /** Removes all Tripetto branding when a valid license is supplied. */
    @Input() removeBranding?: boolean;

    /** Specifies the attachments handler. */
    @Input() attachments?: IRunnerAttachments;

    /** Specifies a custom class name for the HTML element that holds the runner. */
    @Input() className?: string;

    /** Specifies the inline style for the HTML element that holds the runner. */
    @Input() customStyle?: CSSProperties;

    /**
     * Specifies custom CSS rules.
     * To specify rules for a specific block, use this selector: [data-block="<block identifier>"] { ... }
     */
    @Input() customCSS?: string;

    /** Specifies the preferred language (when no language is specified in the definition). */
    @Input() language?: string;

    /** Provides locale information. */
    @Input() locale?: L10n.ILocale | ((locale: string) => L10n.ILocale | Promise<L10n.ILocale | undefined> | undefined);

    /** Provides translations. */
    @Input() translations?:
        | L10n.TTranslation
        | L10n.TTranslation[]
        | ((
              language: string,
              name: string,
              version: string
          ) => L10n.TTranslation | L10n.TTranslation[] | Promise<L10n.TTranslation | L10n.TTranslation[] | undefined> | undefined);

    /** Invoked when the runner is ready. */
    @Output() onReady = new EventEmitter<Instance>();

    /** Invoked when the runner processed a change. */
    @Output() onChange = new EventEmitter<Instance>();

    /** Invoked when data can be imported into the instance. */
    @Output() onImport = new EventEmitter<Instance>();

    /** Invoked when the data in the runner is changed. */
    @Output() onData = new EventEmitter<Instance>();

    /** Specifies a function that is invoked when the user performs an action. */
    @Output() onAction = new EventEmitter<{
        readonly type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete";
        readonly definition: {
            readonly fingerprint: string;
            readonly name: string;
        };
        readonly block?: {
            readonly id: string;
            readonly name: string;
        };
    }>();

    /** Invoked when the runner is about to end and submits data. */
    @Output() onSubmit = new EventEmitter<{
        readonly instance: Instance;
        readonly language: string;
        readonly locale: string;
        readonly namespace: string | undefined;
    }>();

    /** Invoked when the runner is completed (after the data is submitted). */
    @Output() onComplete = new EventEmitter<{
        readonly instance: Instance;
        readonly id?: string;
    }>();

    /** Specifies a function that is invoked when an edit action is requested. */
    @Output() onEdit = new EventEmitter<{
        readonly type: "prologue" | "epilogue" | "styles" | "l10n" | "block";
        readonly id?: string;
    }>();

    /** Specifies a function that is invoked when the runner is "touched" by a user. */
    @Output() onTouch = new EventEmitter<void>();

    /** Invoked when the runner is destroyed. */
    @Output() onDestroy = new EventEmitter<void>();

    /** Specifies a function that is invoked when the runner wants to reload the definition. */
    @Input() onReload?: () => IDefinition | Promise<IDefinition>;

    /** Specifies a function or recipe that is invoked when the runner wants to pause. */
    @Input() onPause?: TAutoscrollPause;

    /** Reference to a builder instance to enable live preview for the builder. */
    @Input() set builder(
        ref:
            | {
                  readonly controller: IBuilderInstance;
              }
            | (() => {
                  readonly controller: IBuilderInstance;
              })
            | undefined
    ) {
        if (!ref) {
            this.builderController = undefined;
        } else {
            new Promise((resolve: (controller: IBuilderInstance) => void) => {
                const fnAwait = () => {
                    if (typeof ref === "function") {
                        const builder = ref();

                        if (builder) {
                            return resolve(builder.controller);
                        }
                    } else if (ref.controller) {
                        return resolve(ref.controller);
                    }

                    requestAnimationFrame(fnAwait);
                };

                fnAwait();
            }).then((controller) => {
                this.builderController = controller;

                controller.hook<
                    "OnChange",
                    IHookPayload<"OnChange"> & {
                        readonly definition: IDefinition;
                    }
                >("OnChange", "synchronous", (changeEvent) => {
                    if (this.runnerController) {
                        this.runnerController.definition = changeEvent.definition;
                    }
                });

                controller.hook<"OnEdit", IHookPayload<"OnEdit"> & { readonly data: TRunnerPreviewData }>(
                    "OnEdit",
                    "synchronous",
                    (editEvent) => {
                        this.runnerController?.doPreview(editEvent.data);
                    }
                );
            });
        }
    }

    get builder() {
        return (
            this.builderController && {
                controller: this.builderController,
            }
        );
    }

    constructor(
        private element: ElementRef,
        private zone: NgZone
    ) {}

    ngOnInit() {
        this.zone.runOutsideAngular(async () => {
            this.runnerController = await run({
                element: this.element.nativeElement,
                definition: this.definition,
                view: this.initialView,
                display: this.display,
                snapshot: this.snapshot,
                styles: this.styles,
                persistent: this.persistent,
                license: this.license,
                removeBranding: this.removeBranding,
                attachments: this.attachments,
                className: this.className,
                customStyle: this.customStyle,
                customCSS: this.customCSS,
                l10n: this.l10n,
                language: this.language,
                locale: this.locale,
                translations: this.translations,
                onReady: (instance) => this.onReady.emit(instance),
                onChange: (instance) => this.onChange.emit(instance),
                onImport: (instance) => this.onImport.emit(instance),
                onData: (instance) => this.onData.emit(instance),
                onAction: (type, definition, block) =>
                    this.onAction.emit({
                        type,
                        definition,
                        block,
                    }),
                onSubmit: (instance, language, locale, namespace) =>
                    this.onSubmit.emit({
                        instance,
                        language,
                        locale,
                        namespace,
                    }),
                onComplete: (instance, id) =>
                    this.onComplete.emit({
                        instance,
                        id,
                    }),
                onEdit: (type, id) => {
                    if (this.builderController) {
                        switch (type) {
                            case "prologue":
                                this.builderController.edit("prologue");
                                break;
                            case "epilogue":
                                this.builderController.edit("epilogue", id);
                                break;
                            case "block":
                                if (id) {
                                    this.builderController.edit("node", id);
                                }
                                break;
                        }
                    }

                    this.onEdit.emit({
                        type,
                        id,
                    });
                },
                onReload: this.onReload,
                onPause: this.onPause,
                onTouch: () => this.onTouch.emit(),
                onDestroy: () => this.onDestroy.emit(),
            });
        });
    }

    ngOnDestroy() {
        if (this.runnerController) {
            this.runnerController.destroy();
            this.runnerController = this.builderController = undefined;
        }
    }
}
