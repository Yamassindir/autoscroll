import { Num, castToBoolean, castToNumber, castToString } from "@tripetto/runner";
import { color } from "@tripetto/runner-fabric/color";
import { useRef } from "react";
import { IAutoscrollStyles } from "@interfaces/styles";
import { SIZE, SMALL_SIZE } from "@ui/const";
import cssesc from "cssesc";

export interface IRuntimeStyles extends IAutoscrollStyles {
    readonly direction: "horizontal" | "vertical";
    readonly verticalAlignment: "top" | "middle" | "bottom";

    readonly font: {
        readonly family: string;
        readonly size: number;
        readonly sizeSmall: number;
        readonly color: string;
    };

    readonly background: {
        readonly color: string;
        readonly opacity: number;
        readonly url?: string;
        readonly positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
    };

    readonly inputs: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
    };

    readonly checkboxes: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
        readonly hideRequiredIndicator: boolean;
    };

    readonly radiobuttons: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly textColor: string;
        readonly scale: number;
    };

    readonly matrix: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
        readonly hideRequiredIndicator: boolean;
    };

    readonly yesNo: {
        readonly yesColor: string;
        readonly noColor: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
        readonly alignment: "horizontal" | "vertical";
    };

    readonly rating: {
        readonly color: string;
        readonly scale: number;
    };

    readonly scale: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
        readonly labelColor: string;
    };

    readonly multipleChoice: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
    };

    readonly pictureChoice: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
    };

    readonly fileUpload: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
    };

    readonly buttons: {
        readonly baseColor: string;
        readonly textColor: string | undefined;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly mode: "fill" | "outline";
        readonly scale: number;
    };

    readonly finishButton: {
        readonly baseColor: string;
        readonly textColor: string | undefined;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly mode: "fill" | "outline";
        readonly scale: number;
    };
}

const generateNavigationStyles = (styles: IAutoscrollStyles, baseColor: string) => {
    const backgroundColor = cssesc(
        styles.navigation?.backgroundColor ||
            (baseColor === "rgb(0, 0, 0)" ? "#f8f9fa" : color(baseColor, (o) => o.manipulate((m) => m.alpha(0.1))))
    );
    const textColor = cssesc(
        styles.navigation?.textColor ||
            (styles.navigation?.backgroundColor && color(styles.navigation?.backgroundColor, (o) => o.makeBlackOrWhite())) ||
            baseColor
    );

    return {
        backgroundColor,
        textColor,
        progressbarColor: cssesc(styles.navigation?.progressbarColor || textColor),
    };
};

const generateRuntimeStyles = (
    styles: IAutoscrollStyles,
    isPage: boolean,
    hasLicense: boolean,
    removeBranding: boolean
): IRuntimeStyles => {
    const backgroundColor = cssesc((styles.background && styles.background.color) || "transparent");
    const textColor = color(styles.color || color(backgroundColor, (o) => o.makeUnclear("#fff").makeBlackOrWhite()), (o) =>
        o.makeUnclear("#000")
    );

    return {
        ...styles,
        ...{
            direction: styles.direction === "horizontal" ? "horizontal" : "vertical",
            verticalAlignment: styles.verticalAlignment || "middle",
            background: {
                color: backgroundColor,
                url:
                    (styles.background &&
                        styles.background.url &&
                        cssesc(styles.background.url, {
                            quotes: "double",
                        })) ||
                    undefined,
                opacity:
                    styles.background?.color && styles.background?.url
                        ? Num.range(castToNumber(styles.background?.opacity, 100) * 0.01, 0, 1)
                        : 1,
                positioning: cssesc((styles.background && styles.background.positioning) || "auto") as
                    | "auto"
                    | "100% auto"
                    | "auto 100%"
                    | "cover"
                    | "contain"
                    | "repeat",
            },
            font: {
                family: cssesc((styles.font && styles.font.family) || "sans-serif"),
                size: (styles.font && styles.font.size && Num.range(castToNumber(styles.font.size, SIZE), 8, 30)) || SIZE,
                sizeSmall:
                    (styles.font && styles.font.sizeSmall && Num.range(castToNumber(styles.font.sizeSmall, SMALL_SIZE), 8, 30)) ||
                    SMALL_SIZE,
                color: textColor,
            },
            inputs: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor: cssesc((styles.inputs && styles.inputs.textColor) || textColor),
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 0.6075 / 0.375,
            },
            checkboxes: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor,
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
                hideRequiredIndicator: styles.hideRequiredIndicator || false,
            },
            radiobuttons: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                textColor,
                scale: 1,
            },
            matrix: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                textColor,
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
                hideRequiredIndicator: styles.hideRequiredIndicator || false,
            },
            yesNo: {
                yesColor: cssesc((styles.inputs && styles.inputs.agreeColor) || "green"),
                noColor: cssesc((styles.inputs && styles.inputs.declineColor) || "red"),
                alignment: "horizontal",
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            rating: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                scale: 2,
            },
            scale: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
                labelColor: textColor,
            },
            multipleChoice: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            pictureChoice: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            fileUpload: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor: cssesc((styles.inputs && styles.inputs.textColor) || textColor),
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
            },
            buttons: {
                baseColor: cssesc((styles.buttons && styles.buttons.baseColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.buttons?.roundness,
                mode: styles.buttons && styles.buttons.mode === "outline" ? "outline" : "fill",
                textColor: (styles.buttons && styles.buttons.textColor && cssesc(styles.buttons.textColor)) || undefined,
                scale: 0.6075 / 0.375,
            },
            finishButton: {
                baseColor: cssesc((styles.buttons && (styles.buttons.finishColor || styles.buttons.baseColor)) || "green"),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.buttons?.roundness,
                textColor:
                    (styles.buttons && !styles.buttons.finishColor && styles.buttons.textColor && cssesc(styles.buttons.textColor)) ||
                    undefined,
                mode: "fill",
                scale: 0.6075 / 0.375,
            },
            navigation:
                styles.showNavigation === "always" || (isPage && castToString(styles.showNavigation, "auto") === "auto")
                    ? generateNavigationStyles(styles, textColor)
                    : undefined,
            showProgressbar: castToBoolean(styles.showProgressbar, true),
            showSeparateSubmit: castToBoolean(styles.showSeparateSubmit, true),
            showPreviousButton: castToBoolean(styles.showPreviousButton, true),
            showScrollbar: styles.direction === "vertical" && !styles.disableScrolling && styles.showScrollbar,
            noBranding: (hasLicense && (removeBranding || styles.noBranding)) || false,
        },
    };
};

export const useStyles = (
    initialStyles: IAutoscrollStyles | undefined,
    isPage: boolean,
    hasLicense: boolean,
    removeBranding: boolean,
    onChange: () => void
) => {
    const designtimeStyles = useRef<IAutoscrollStyles>(initialStyles || {});
    const runtimeStyles = useRef<IRuntimeStyles>();

    if (!runtimeStyles.current) {
        runtimeStyles.current = generateRuntimeStyles(designtimeStyles.current, isPage, hasLicense, removeBranding);
    }

    return [
        designtimeStyles.current,
        runtimeStyles.current,
        (styles: IAutoscrollStyles) => {
            designtimeStyles.current = styles;
            runtimeStyles.current = generateRuntimeStyles(styles, isPage, hasLicense, removeBranding);

            onChange();
        },
    ] as [IAutoscrollStyles, IRuntimeStyles, (styles: IAutoscrollStyles) => void];
};
