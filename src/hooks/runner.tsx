import { MutableRefObject, WheelEvent, useEffect, useRef, useState } from "react";
import {
    DateTime,
    IObservableNode,
    Num,
    cancelFrame,
    castToBoolean,
    compare,
    each,
    findFirst,
    markdownifyToString,
    markdownifyToURL,
    scheduleAction,
    scheduleFrame,
    set,
} from "@tripetto/runner";
import { markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IAutoscrollUIProps } from "@interfaces/props";
import { IAutoscrollRendering } from "@interfaces/block";
import { IViewport } from "@interfaces/viewport";
import { IElement } from "@interfaces/element";
import { IRuntimeStyles } from "./styles";
import { useOverlay } from "@tripetto/runner-fabric/overlay";
import { RequiredIndicatorFabric } from "@tripetto/runner-fabric/components/required-indicator";
import { warningIcon } from "@tripetto/runner-fabric/icons/warning";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { useAutoscrollController } from "./controller";
import { useFocus } from "./focus";
import { isLoading } from "@helpers/resizer";
import { isIn } from "@helpers/isin";
import { Block, Blocks } from "@ui/blocks";
import { BlockTitle } from "@ui/blocks/title";
import { BlockEnumerator } from "@ui/blocks/enumerator";
import { BlockDescription } from "@ui/blocks/description";
import { BlockExplanation } from "@ui/blocks/explanation";
import { BlockButtons } from "@ui/blocks/buttons";
import { BLOCK_PAUSE, PauseBlock } from "@ui/blocks/pause";
import { BLOCK_SUBMIT, SubmitBlock } from "@ui/blocks/submit";
import { SubmitIcon } from "@ui/icons/submit";
import { ArrowRightIcon } from "@ui/icons/arrow-right";
import { ArrowDownIcon } from "@ui/icons/arrow-down";
import { PauseIcon } from "@ui/icons/pause";
import { OutdatedError } from "@ui/errors/outdated";
import { RejectedError } from "@ui/errors/rejected";
import { ConnectionError } from "@ui/errors/connection";
import { PauseError } from "@ui/errors/pause";
import { EvaluatingMessage } from "@ui/messages/evaluating";
import { ErrorMessage } from "@ui/errors";
import { PreviewMessage } from "@ui/messages/preview";
import { NAVIGATION_HEIGHT } from "@ui/const";

interface ICalculated {
    offset: number;
    size: number;
    height: number;
    active?: string;

    offsets?: {
        [key: string]: number | undefined;
    };
}

const calculate = (
    viewport: IViewport,
    styles: IRuntimeStyles,
    elements: IElement[],
    current: Readonly<ICalculated>,
    setResult: (result: Readonly<ICalculated>) => void,
    isCalculated?: MutableRefObject<boolean>
) => {
    const result: ICalculated = {
        offset: 0,
        size: 0,
        height: current.height,
    };

    let width = 0;
    let height = 0;
    let minHeight = 0;

    each(elements, (element) => {
        if (styles.direction === "horizontal") {
            width += element.width;

            if (styles.verticalAlignment !== "top") {
                (result.offsets || (result.offsets = {}))[element.key] =
                    element.height < viewport.height
                        ? Num.floor((viewport.height - element.height) / (styles.verticalAlignment === "bottom" ? 1 : 2))
                        : 0;
            }
        }

        if (styles.direction === "vertical") {
            height += element.height;

            if (element.height > result.height) {
                result.height = element.height;
            }

            if (!minHeight || element.height < minHeight) {
                minHeight = element.height;
            }
        }
    });

    if (height === 0 && width === 0) {
        return;
    }

    if (styles.direction === "vertical" && height > 0) {
        const first = elements[0];
        const last = elements[elements.length - 1];
        const alignment = styles.verticalAlignment;

        if (elements.length > 1 && alignment !== "bottom" && last && last.height < viewport.height) {
            result.size = height + (viewport.height - last.height) / (alignment === "top" ? 1 : 2);
        }

        if (alignment === "bottom") {
            const offset = first.height < viewport.height ? Num.floor(viewport.height - first.height) : 0;
            const median = viewport.height - minHeight / 2;
            let y = offset - viewport.scrollTop;

            const active =
                (elements.length > 1 &&
                    findFirst(elements, (element) => {
                        if ((element.isFirst || median >= y) && (element.isLast || median < y + element.height)) {
                            return true;
                        }

                        y += element.height;

                        return false;
                    })) ||
                first;

            result.offset = offset;
            result.active = active.key;
        } else if (alignment === "top") {
            let y = 0;
            const active =
                (elements.length > 1 &&
                    findFirst(elements, (element) => {
                        if (
                            (element.isFirst || viewport.scrollTop >= y - element.height / 2) &&
                            (element.isLast ||
                                viewport.scrollTop <
                                    Num.max(y + element.height / 2, y + element.height - elements[element.index + 1].height / 2))
                        ) {
                            return true;
                        }

                        y += element.height;

                        return false;
                    })) ||
                first;

            result.active = active.key;
        } else {
            const median = viewport.height / 2;
            const offset = first.height < viewport.height ? Num.floor((viewport.height - first.height) / 2) : 0;
            let y = offset - viewport.scrollTop;
            const active =
                (elements.length > 1 &&
                    findFirst(elements, (element) => {
                        if ((element.isFirst || median >= y) && (element.isLast || median < y + element.height)) {
                            return true;
                        }

                        y += element.height;

                        return false;
                    })) ||
                first;

            result.offset = offset;
            result.active = active.key;
        }
    } else if (styles.direction === "horizontal" && width > 0) {
        let x = -viewport.scrollLeft;
        const first = elements[0];
        const median = viewport.width / 2;

        result.active = (
            (elements.length > 1 &&
                findFirst(elements, (element) => {
                    if ((element.isFirst || median >= x) && (element.isLast || median < x + element.width)) {
                        return true;
                    }

                    x += element.width;

                    return false;
                })) ||
            first
        ).key;
    }

    if (!isCalculated || !isCalculated.current || !compare(current, result, true)) {
        if (isCalculated && !isCalculated.current) {
            isCalculated.current = true;
        }

        setResult(result);
    }
};

const scrollToBlock = (viewport: IViewport, styles: IRuntimeStyles, elements: IElement[], key: string) => {
    if (viewport.scrollTo) {
        const dest = findFirst(elements, (element) => element.key === key);

        if (dest && dest.height > 0) {
            const factor = styles.verticalAlignment === "bottom" ? 1 : 2;
            const left = styles.direction === "horizontal" ? dest.left : 0;
            const top =
                styles.direction === "horizontal"
                    ? 0
                    : styles.verticalAlignment === "top" || dest.height >= viewport.height
                    ? dest.top
                    : dest.top + dest.height / factor - viewport.height / factor;

            scheduleAction(() => viewport.scrollTo!(left, top));

            return true;
        }
    }

    return false;
};

const scroll = (viewport: IViewport, styles: IRuntimeStyles, elements: IElement[], direction: "previous" | "next", key: string) => {
    const current = findFirst(elements, (element) => element.key === key);

    if (current) {
        if (direction === "previous" && current.isFirst) {
            return false;
        }

        if (direction === "next" && current.isLast) {
            return false;
        }

        return scrollToBlock(viewport, styles, elements, elements[current.index + (direction === "previous" ? -1 : 1)].key);
    }

    return false;
};

const scrollToPrevious = (viewport: IViewport, styles: IRuntimeStyles, elements: IElement[], key: string) =>
    scroll(viewport, styles, elements, "previous", key);
const scrollToNext = (viewport: IViewport, styles: IRuntimeStyles, elements: IElement[], key: string) =>
    scroll(viewport, styles, elements, "next", key);

const scrollToFirst = (viewport: IViewport, styles: IRuntimeStyles, elements: IElement[]) => {
    if (elements.length > 0) {
        scrollToBlock(viewport, styles, elements, elements[0].key);
    }
};

export const useAutoscrollRunner = (props: IAutoscrollUIProps) => {
    const calculatedRef = useRef(false);
    const loadingRef = useRef(false);

    const [calculated, setCalculated] = useState<Readonly<ICalculated>>({
        offset: 0,
        size: 0,
        height: 0,
    });

    const viewportRef = useRef<IViewport>({
        timestamp: 0,
        left: 0,
        top: 0,
        width: 0,
        height: 0,
        scrollLeft: 0,
        scrollTop: 0,
    });

    const activateRef = useRef(props.snapshot?.b?.a);
    const actionRef = useRef("");
    const submitRef = useRef<HTMLElement>();
    const [runner, cache, l10n, styles, attachments, doAction] = useAutoscrollController({
        ...props,
        onSnapshot: () => ({
            a: calculated.active,
            b,
            d: props.snapshot && props.snapshot.b && props.snapshot.b.d,
            e: DateTime.now,
        }),
        onSnapshotStart: () => (activateRef.current = BLOCK_PAUSE),
        onSnapshotEnd: (prev?: string) => scrollToBlock(viewportRef.current, styles, elementsRef.current, prev || ""),
        onStart: () => {
            if (viewportRef.current.scrollTo) {
                viewportRef.current.scrollTo(0, 0, false);
            }
        },
        onRestart: () => {
            resetFocus();

            if (viewportRef.current.scrollTo) {
                viewportRef.current.scrollTo(0, 0, false);
            }
        },
        onScrollIntoView: (id) => {
            const node = findFirst(runner.storyline?.nodes, (n) => n.node.id === id);

            if (node) {
                return scrollToBlock(viewportRef.current, styles, elementsRef.current, node.key);
            }

            return false;
        },
    });

    const directionRef = useRef(styles.direction);
    const status = runner.status;
    const view = runner.view;
    const isPage = view !== "live" || props.display === "page";
    const isEvaluating =
        (runner.storyline && view !== "preview" && (runner.storyline.isEvaluating || (runner.pausing && runner.pausing.isCompleting))) ||
        false;

    const [overlayProvider, overlay] = useOverlay();
    const [frameRef, handleFocus, handleAutoFocus, hasOrHadFocus, blur, resetFocus, b] = useFocus({
        gainFocus: view === "live" && (props.display === "page" || styles.autoFocus),
        initialFocus: props.snapshot && props.snapshot.b && props.snapshot.b.b,
        viewportRef,
        onFocus: (key: string) => {
            if (calculated.active !== key) {
                scrollToBlock(viewportRef.current, styles, elementsRef.current, key);
            }
        },
    });

    const tabRef = useRef<
        {
            node: string;
            first: number;
            last: number;
        }[]
    >([]);

    const elementsRef = useRef<IElement[]>([]);
    const addElement = (key: string) => (ref: HTMLElement | null) => {
        if (ref) {
            const index = elementsRef.current.length;

            elementsRef.current.push({
                index,
                key,
                ref,
                width: 0,
                height: 0,
                get left() {
                    if (styles.direction === "horizontal") {
                        let i = index;
                        let left = 0;

                        while (--i >= 0) {
                            left += elementsRef.current[i].width;
                        }

                        return left;
                    } else {
                        return 0;
                    }
                },
                get top() {
                    if (styles.direction === "vertical") {
                        let i = index;
                        let top = calculated.offset;

                        while (--i >= 0) {
                            top += elementsRef.current[i].height;
                        }

                        return top;
                    } else {
                        return 0;
                    }
                },
                get isFirst() {
                    return index === 0;
                },
                get isLast() {
                    return index === elementsRef.current.length - 1;
                },
            });
        }
    };

    elementsRef.current = [];

    if (directionRef.current !== styles.direction) {
        calculatedRef.current = false;
        directionRef.current = styles.direction;
    }

    if (!loadingRef.current && calculatedRef.current) {
        loadingRef.current = !isLoading();
    }

    useEffect(() => {
        let activationHandle = 0;

        each(elementsRef.current, (element) => {
            const rect = element.ref.getBoundingClientRect();

            element.width = rect.width;
            element.height = rect.height;
        });

        calculate(viewportRef.current, styles, elementsRef.current, calculated, setCalculated, calculatedRef);

        if (activateRef.current) {
            const activateBlock = () => {
                const current = activateRef.current;

                if (current) {
                    const dest = current && findFirst(elementsRef.current, (element) => element.key === current);

                    if (dest && dest.height > 0) {
                        scrollToBlock(viewportRef.current, styles, elementsRef.current, current);

                        activateRef.current = undefined;
                    } else {
                        activationHandle = scheduleFrame(() => activateBlock());
                    }
                }
            };

            activateBlock();
        }

        return () => {
            if (activationHandle) {
                cancelFrame(activationHandle);
            }
        };
    });

    let active = 0;
    let total = 0;
    let progress = 0;
    let answerable = 0;
    let answered = 0;
    let failed = 0;
    let failedFirst = "";
    let activate: IObservableNode<IAutoscrollRendering> | undefined;
    let deactivate: IObservableNode<IAutoscrollRendering> | undefined;
    const showSubmitBlock = styles.showSeparateSubmit && runner.isFinishable;
    const nodes = runner.storyline?.all || [];
    const blocks =
        (runner.preview !== "epilogue" && nodes.length > 0 && (
            <Blocks
                style={{
                    marginTop: (styles.direction === "vertical" && calculated.offset) || undefined,
                    minHeight: (styles.direction === "vertical" && calculated.size) || undefined,
                }}
                onWheel={(wheelEvent: WheelEvent<HTMLDivElement>) => {
                    if (styles.direction === "horizontal" && styles.showPreviousButton && calculated.active) {
                        const offset = (calculated.offsets && calculated.offsets[calculated.active]) || 0;

                        if (offset > 0 && wheelEvent.deltaY < 0) {
                            scrollToPrevious(viewportRef.current, styles, elementsRef.current, calculated.active);
                        }
                    }
                }}
                $styles={styles}
                $view={view}
                $isPage={isPage}
                $isCalculated={calculatedRef.current && loadingRef.current}
            >
                {nodes.map((node) => {
                    const storyline = runner.storyline!;
                    const index = total++;
                    const isFirst = index === 0;
                    const isLast = nodes.length === total;
                    const isActivated = calculated.active === node.key && status !== "pausing";
                    const isLocked = status === "finishing" || status === "pausing";
                    const isHidden =
                        !isActivated &&
                        ((styles.hidePast && active === 0) || (styles.hideUpcoming && ((!calculated.active && index > 0) || active !== 0)));
                    const fnEdit = (props.onEdit && view !== "live" && node.id && (() => props.onEdit!("block", node.id))) || undefined;
                    const fnPrevious =
                        (!isFirst && (() => scrollToPrevious(viewportRef.current, styles, elementsRef.current, node.key))) || undefined;
                    const fnNext =
                        ((!isLast || showSubmitBlock) &&
                            (() => scheduleAction(() => scrollToNext(viewportRef.current, styles, elementsRef.current, node.key)))) ||
                        undefined;
                    const fnSubmit =
                        (!styles.showSeparateSubmit && isLast && runner.isFinishable && (() => storyline.finish())) || undefined;
                    const ariaDescription = () =>
                        (node.props.explanation && (
                            <BlockExplanation id={node.block?.key("explanation")} onClick={fnEdit}>
                                {markdownifyToJSX(node.props.explanation, node.context)}
                            </BlockExplanation>
                        )) ||
                        undefined;
                    const previousTab = (index > 0 && index - 1 < tabRef.current.length && tabRef.current[index - 1]) || undefined;
                    let currentTab = (index < tabRef.current.length && tabRef.current[index]) || undefined;

                    if (isActivated && actionRef.current !== node.key) {
                        activate = node;
                    } else if (!isActivated && actionRef.current === node.key) {
                        deactivate = node;
                    }

                    if (!currentTab || currentTab.node !== node.key || (previousTab && previousTab.last !== currentTab.first)) {
                        currentTab = {
                            node: node.key,
                            first: previousTab?.last || 0,
                            last: previousTab?.last || 0,
                        };

                        tabRef.current.splice(index, tabRef.current.length - index, currentTab);
                    }

                    const block = cache.fetch(
                        () => {
                            const name = () =>
                                (node.props.name && castToBoolean(node.props.nameVisible, true) && (
                                    <BlockTitle onClick={fnEdit}>
                                        {styles.showEnumerators && node.enumerator && (
                                            <BlockEnumerator $view={view}>{node.enumerator}.</BlockEnumerator>
                                        )}
                                        {node.block ? (
                                            <label htmlFor={node.block?.key()}>{markdownifyToJSX(node.props.name, node.context)}</label>
                                        ) : (
                                            <>{markdownifyToJSX(node.props.name, node.context)}</>
                                        )}
                                        {!styles.hideRequiredIndicator &&
                                            node.block?.required &&
                                            !node.block.hideRequiredIndicatorFromName && (
                                                <RequiredIndicatorFabric $errorColor={styles.inputs.errorColor} />
                                            )}
                                    </BlockTitle>
                                )) ||
                                undefined;

                            const description = () =>
                                (node.props.description && (
                                    <BlockDescription onClick={fnEdit}>
                                        {markdownifyToJSX(node.props.description, node.context)}
                                    </BlockDescription>
                                )) ||
                                undefined;

                            currentTab!.last = currentTab!.first;

                            return (
                                (node.block?.render &&
                                    node.block?.render(
                                        {
                                            l10n,
                                            styles,
                                            overlay,
                                            view,
                                            attachments,
                                            index,
                                            get id() {
                                                return node.block?.key() || "";
                                            },
                                            get name() {
                                                return name();
                                            },
                                            get description() {
                                                return description();
                                            },
                                            get explanation() {
                                                return (
                                                    (node.props.explanation && markdownifyToJSX(node.props.explanation, node.context)) ||
                                                    undefined
                                                );
                                            },
                                            get placeholder() {
                                                return markdownifyToString(node.props.placeholder || "", node.context) || "";
                                            },
                                            get label() {
                                                return markdownifyToJSX(
                                                    node.props.placeholder || node.props.name || "...",
                                                    node.context,
                                                    false
                                                );
                                            },
                                            get tabIndex() {
                                                return ++currentTab!.last;
                                            },
                                            isActivated,
                                            get isFailed() {
                                                return (view !== "preview" && node.isFailed && hasOrHadFocus(node) !== undefined) || false;
                                            },
                                            get ariaDescribedBy() {
                                                return (node.props.explanation && node.block?.key("explanation")) || undefined;
                                            },
                                            get ariaDescription() {
                                                return ariaDescription();
                                            },
                                            isPage,
                                            focus: handleFocus(node, true, () => doAction("focus", node)),
                                            blur: handleFocus(node, false, () => doAction("blur", node)),
                                            autoFocus: handleAutoFocus(node, isActivated),
                                            markdownifyToJSX: (md: string, lineBreaks?: boolean) =>
                                                markdownifyToJSX(md, node.context, lineBreaks),
                                            markdownifyToURL: (md: string) => markdownifyToURL(md, node.context),
                                            markdownifyToImage: (md: string) =>
                                                markdownifyToURL(md, node.context, undefined, [
                                                    "image/jpeg",
                                                    "image/png",
                                                    "image/svg",
                                                    "image/gif",
                                                ]) || `data:image/svg+xml;base64,${warningIcon(styles.color || "#000")}`,
                                            markdownifyToString: (md: string, lineBreaks?: boolean) =>
                                                markdownifyToString(md, node.context, undefined, lineBreaks),
                                        },
                                        (status !== "finishing" &&
                                            status !== "pausing" &&
                                            status !== "evaluating" &&
                                            node.isPassed &&
                                            fnNext &&
                                            (() => blur().then(fnNext))) ||
                                            (fnSubmit &&
                                                (() => {
                                                    if (submitRef.current && isIn(submitRef.current, viewportRef)) {
                                                        submitRef.current.focus({
                                                            preventScroll: true,
                                                        });
                                                    }
                                                })),
                                        fnPrevious
                                    )) || (
                                    <>
                                        {name()}
                                        {description()}
                                    </>
                                )
                            );
                        },
                        node,
                        status,
                        isActivated,
                        currentTab.first
                    );

                    if (isActivated) {
                        active = total;
                    }

                    if (
                        (!active || isActivated) &&
                        (view === "preview" ||
                            !node.collectsData ||
                            (node.hasDataCollected && !node.isFailed) ||
                            (!active && !node.block?.required))
                    ) {
                        progress++;
                    }

                    if (node.collectsData) {
                        answerable++;

                        if ((!active || (isActivated && node.hasDataCollected)) && !node.isFailed) {
                            answered++;
                        }

                        if (!active && view !== "preview" && node.isFailed) {
                            failed++;

                            if (!failedFirst) {
                                failedFirst = node.key;
                            }
                        }
                    }

                    return (
                        block && (
                            <Block
                                key={node.key}
                                identifier={node.block?.type.identifier}
                                element={addElement(node.key)}
                                l10n={l10n}
                                styles={styles}
                                view={view}
                                isPage={isPage}
                                bookmark={node.bookmark.name}
                                isActivated={isActivated}
                                isLocked={isLocked}
                                isHidden={isHidden}
                                width={(styles.direction === "horizontal" && viewportRef.current.width) || undefined}
                                top={calculated.offsets && calculated.offsets[node.key]}
                                showBanner={((isFirst || (isLast && fnSubmit)) && isActivated) || false}
                                onActivate={() => scrollToBlock(viewportRef.current, styles, elementsRef.current, node.key)}
                                onPrevious={fnPrevious}
                            >
                                {block}
                                {!node.block?.hideAriaDescription && ariaDescription()}
                                {(!node.block?.hideButtons ||
                                    isEvaluating ||
                                    (fnSubmit && runner.isFinishable) ||
                                    (!styles.navigation && runner.isPausable)) && (
                                    <BlockButtons styles={styles} isActivated={isActivated}>
                                        {isEvaluating ? (
                                            <EvaluatingMessage l10n={l10n} styles={styles} />
                                        ) : (
                                            <>
                                                {((!fnSubmit && !node.block?.autoSubmit) || (fnSubmit && runner.isFinishable)) && (
                                                    <ButtonFabric
                                                        styles={
                                                            fnSubmit || (node.isPassed && isLast && !styles.showSeparateSubmit)
                                                                ? styles.finishButton
                                                                : styles.buttons
                                                        }
                                                        disabled={
                                                            status === "finishing" ||
                                                            status === "pausing" ||
                                                            !node.isPassed ||
                                                            (fnSubmit &&
                                                                (view === "preview" ||
                                                                    (view === "live" && !props.onSubmit && storyline.hasDataCollected)))
                                                        }
                                                        icon={
                                                            fnSubmit || (node.isPassed && isLast && !styles.showSeparateSubmit)
                                                                ? SubmitIcon
                                                                : styles.direction === "horizontal"
                                                                ? ArrowRightIcon
                                                                : ArrowDownIcon
                                                        }
                                                        label={
                                                            fnSubmit || (node.isPassed && isLast && !styles.showSeparateSubmit)
                                                                ? status === "finishing"
                                                                    ? l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                                                    : l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                                                                : l10n.pgettext("runner#1|🆗 Buttons", "Next")
                                                        }
                                                        onAutoFocus={
                                                            ((!node.block || node.block.autoFocus) &&
                                                                handleAutoFocus(node, isActivated, (el) => {
                                                                    if (fnSubmit) {
                                                                        submitRef.current = el;
                                                                    }
                                                                })) ||
                                                            ((el) => {
                                                                if (fnSubmit && el) {
                                                                    submitRef.current = el;
                                                                }
                                                            })
                                                        }
                                                        onClick={fnSubmit || fnNext}
                                                        onTab={(!fnSubmit && fnNext) || undefined}
                                                        onCancel={
                                                            ((!node.block || node.block.autoFocus || fnSubmit) && fnPrevious) || undefined
                                                        }
                                                    />
                                                )}
                                                {!styles.navigation && runner.isPausable && (
                                                    <ButtonFabric
                                                        styles={{ ...styles.buttons, mode: "outline" }}
                                                        icon={PauseIcon}
                                                        onClick={runner.pause}
                                                    />
                                                )}
                                                {(fnSubmit || (isLast && !styles.showSeparateSubmit)) && view === "preview" && (
                                                    <PreviewMessage l10n={l10n} styles={styles} />
                                                )}
                                            </>
                                        )}
                                    </BlockButtons>
                                )}
                            </Block>
                        )
                    );
                })}
                {runner.pausing || calculated.active === BLOCK_PAUSE ? (
                    <PauseBlock
                        controller={runner.pausing}
                        view={view}
                        l10n={l10n}
                        styles={styles}
                        isPage={isPage}
                        onElement={addElement}
                        onAutoFocus={handleAutoFocus}
                    />
                ) : (
                    showSubmitBlock && (
                        <SubmitBlock
                            view={view}
                            l10n={l10n}
                            styles={styles}
                            isPage={isPage}
                            status={status}
                            active={calculated.active}
                            onElement={addElement}
                            width={(styles.direction === "horizontal" && viewportRef.current.width) || undefined}
                            offset={calculated.offsets && calculated.offsets[BLOCK_SUBMIT]}
                            onActivate={() => scrollToBlock(viewportRef.current, styles, elementsRef.current, BLOCK_SUBMIT)}
                            onSubmit={
                                view !== "live" || props.onSubmit || !(runner.storyline && runner.storyline.hasDataCollected)
                                    ? () => runner.storyline!.finish()
                                    : undefined
                            }
                            onTab={() => scrollToFirst(viewportRef.current, styles, elementsRef.current)}
                            onPrevious={() => scrollToPrevious(viewportRef.current, styles, elementsRef.current, BLOCK_SUBMIT)}
                            onAutoFocus={handleAutoFocus}
                        />
                    )
                )}
            </Blocks>
        )) ||
        undefined;

    if (showSubmitBlock) {
        total++;

        if (calculated.active === BLOCK_SUBMIT) {
            active = progress = total;
        }
    }

    if (!active) {
        failed = 0;
        answered = 0;
        progress = 0;
    }

    if (deactivate) {
        doAction("unstage", deactivate);

        actionRef.current = "";
    }

    if (activate) {
        doAction("stage", activate);

        actionRef.current = activate.key;
    }

    const previous =
        (status !== "pausing" &&
            active > 1 &&
            (() => calculated.active && scrollToPrevious(viewportRef.current, styles, elementsRef.current, calculated.active))) ||
        undefined;
    const next =
        (status !== "pausing" &&
            active < total &&
            (() => calculated.active && scrollToNext(viewportRef.current, styles, elementsRef.current, calculated.active))) ||
        undefined;

    return {
        frameRef,
        status,
        view,
        isPage,
        title: runner.title,
        l10n,
        styles,
        overlayProvider,
        height:
            isPage || (props.customStyle && props.customStyle.height && props.customStyle.height !== "auto")
                ? undefined
                : styles.direction === "vertical" && blocks
                ? calculated.height + (styles.navigation ? NAVIGATION_HEIGHT : 0)
                : ("auto" as const),
        disableScrolling:
            (styles.direction === "horizontal" &&
                !runner.prologue &&
                !runner.epilogue &&
                calculated.active &&
                calculated.offsets &&
                (calculated.offsets[calculated.active] || 0) > 0) ||
            false,
        prologue: runner.prologue && {
            ...runner.prologue,
            l10n,
            styles,
            view,
            isPage,
            viewportRef,
            start: runner.preview === "prologue" ? runner.resetPreview : runner.start,
            edit: (view !== "live" && props.onEdit && (() => props.onEdit!("prologue"))) || undefined,
        },
        blocks: blocks && (
            <>
                <ErrorMessage
                    l10n={l10n}
                    styles={styles}
                    failed={failed}
                    error={
                        status === "error" ? (
                            <ConnectionError
                                l10n={l10n}
                                styles={styles}
                                onRetry={() => runner.storyline!.finish()}
                                onDiscard={() => runner.discard()}
                            />
                        ) : status === "error-outdated" ? (
                            <OutdatedError
                                l10n={l10n}
                                styles={styles}
                                onReload={
                                    props.onReload &&
                                    (async () => {
                                        runner.reload(await Promise.resolve(props.onReload!()));
                                        runner.discard();
                                    })
                                }
                                onDiscard={() => runner.discard()}
                            />
                        ) : status === "error-rejected" ? (
                            <RejectedError l10n={l10n} styles={styles} onDiscard={() => runner.discard()} />
                        ) : status === "error-paused" ? (
                            <PauseError l10n={l10n} styles={styles} onDiscard={() => runner.discard()} />
                        ) : undefined
                    }
                    onClick={
                        (status !== "error" &&
                            status !== "error-outdated" &&
                            status !== "error-rejected" &&
                            status !== "error-paused" &&
                            (() => scrollToBlock(viewportRef.current, styles, elementsRef.current, failedFirst))) ||
                        undefined
                    }
                />
                {blocks}
            </>
        ),
        epilogue: runner.epilogue && {
            ...runner.epilogue,
            l10n,
            styles,
            view,
            isPage,
            repeat: runner.restart,
            edit: (view !== "live" && props.onEdit && (() => props.onEdit!("epilogue", runner.epilogue?.branch))) || undefined,
        },
        isEvaluating,
        navigation:
            styles.navigation && blocks
                ? {
                      view,
                      l10n,
                      styles,
                      total,
                      progress,
                      answerable,
                      answered,
                      isFinishing: status === "finishing",
                      isPausing: status === "pausing",
                      isEvaluating,
                      previous,
                      next,
                      pausable: runner.isPausable,
                      pause: runner.pause,
                  }
                : undefined,
        onResize: () => runner.update(),
        onScroll: (viewport: IViewport) => {
            /**
             * Here we check if one of the viewport dimension has changed. If an
             * input control has focus, this is probably caused by a
             * soft-keyboard popping up. We should maintain the viewport
             * dimensions in order to avoid any rendering glitches.
             */
            if (
                ((viewportRef.current.width === viewport.width && viewportRef.current.height !== viewport.height) ||
                    (viewportRef.current.width !== viewport.width && viewportRef.current.height === viewport.height)) &&
                calculated.active &&
                hasOrHadFocus({ key: calculated.active, filterInputs: true })
            ) {
                set(viewport, "width", viewportRef.current.width);
                set(viewport, "height", viewportRef.current.height);
            }

            if (viewportRef.current.width !== viewport.width || viewportRef.current.height !== viewport.height) {
                runner.update();
            }

            if (viewportRef.current.scrollLeft !== viewport.scrollLeft || viewportRef.current.scrollTop !== viewport.scrollTop) {
                calculate(viewport, styles, elementsRef.current, calculated, setCalculated, calculatedRef);
            }

            viewportRef.current = viewport;
        },
        onKey: (action: "previous" | "next") => {
            if (status !== "finishing" && status !== "pausing" && status !== "evaluating") {
                switch (action) {
                    case "previous":
                        if (previous) {
                            previous();
                        }
                        break;
                    case "next":
                        if (next) {
                            next();
                        }
                        break;
                }
            }
        },
    };
};
