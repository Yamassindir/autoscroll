import { MutableRefObject, useEffect, useRef, useState } from "react";
import { TRunnerPreviewData, TRunnerViews, useL10n, useRunner } from "@tripetto/runner-react-hook";
import {
    IDefinition,
    ISnapshot,
    Instance,
    L10n,
    TL10n,
    findLast,
    isBoolean,
    isFunction,
    scheduleAction,
    set,
    verify,
} from "@tripetto/runner";
import { namespace } from "@namespace";
import { IAutoscrollRendering } from "@interfaces/block";
import { IAutoscrollStyles } from "@interfaces/styles";
import { IAutoscrollUIProps } from "@interfaces/props";
import { IAutoscrollSnapshot } from "@interfaces/snapshot";
import { IPausingRecipeEmail } from "@interfaces/pausing";
import { useCache } from "./cache";
import { useStyles } from "./styles";
import { EmailBlock } from "@blocks/email/email";

declare const PACKAGE_NAME: string;

export interface IAutoscrollController {
    definition: IDefinition;
    styles: IAutoscrollStyles;
    l10n: TL10n;
    view: TRunnerViews;
    readonly instance: Instance | undefined;
    readonly fingerprint: string;
    readonly snapshot: ISnapshot<IAutoscrollSnapshot> | undefined;
    readonly isRunning: boolean;
    readonly isFinishing: boolean;
    readonly isPausing: boolean;
    readonly isLicensed: boolean;
    readonly allowStart: boolean;
    readonly allowRestart: boolean;
    readonly allowPause: boolean;
    readonly allowStop: boolean;
    readonly start: () => void;
    readonly restart: () => void;
    readonly pause: () => ISnapshot<IAutoscrollSnapshot> | Promise<ISnapshot<IAutoscrollSnapshot>> | undefined;
    readonly stop: () => void;
    readonly doPreview: (data: TRunnerPreviewData) => void;
}

export const useAutoscrollController = (
    props: IAutoscrollUIProps & {
        readonly onSnapshot: (type: "pause" | "snapshot") => IAutoscrollSnapshot;
        readonly onSnapshotStart: () => void;
        readonly onSnapshotEnd: (prev?: string) => void;
        readonly onStart?: () => void;
        readonly onRestart?: () => void;
        readonly onScrollIntoView?: (id: string) => boolean;
    }
) => {
    const runner = useRunner<IAutoscrollRendering>(props, {
        namespace,
        mode: "ahead",
        onPreview: (action: "start" | "end", type: "prologue" | "block" | "epilogue", id?: string) => {
            if (action === "start" && props.onScrollIntoView && type === "block" && id && !props.onScrollIntoView(id)) {
                previewRef.current = id;
            } else if (action === "end") {
                previewRef.current = undefined;
            }
        },
        onRestart: () => {
            if (onRestartRef.current) {
                onRestartRef.current();
            }
        },
        onDestroy: () => {
            if (props.controller?.current) {
                props.controller.current = undefined;
            }
        },
    });
    const cache = useCache();
    const [hasLicense] = useState(() => (props.license && verify(PACKAGE_NAME, props.license)) || false);
    const [styles, runtimeStyles, setStyles] = useStyles(
        props.styles,
        runner.view !== "live" || props.display === "page",
        hasLicense,
        props.removeBranding || false,
        () => {
            cache.purge();
            runner.update();
        }
    );
    const [l10n, setL10n, updateL10n] = useL10n(props.l10n, props.onL10n, runner, () => cache.purge());
    const controllerRef = useRef<IAutoscrollController>();
    const onRestartRef = useRef<() => void>();
    const previewRef = useRef<string>();
    const snapshotRef = useRef<ISnapshot<IAutoscrollSnapshot>>();
    const pauseRef = useRef<IPausingRecipeEmail>();

    const pause = () => {
        if (props.onPause) {
            return runner.pause<IAutoscrollSnapshot>(
                props.onSnapshot("pause"),
                (
                    snapshot: ISnapshot<IAutoscrollSnapshot>,
                    cb: (result: "succeeded" | "failed" | "canceled", retry?: () => void) => void
                ) => {
                    const done = (result: "succeeded" | "failed" | "canceled", retry?: () => void) => {
                        if (pauseRef.current) {
                            props.onSnapshotEnd(snapshot.b?.a);
                        }

                        pauseRef.current = undefined;

                        cb(result, retry);
                    };
                    const handlePause = (handler: () => boolean | void | Promise<void>) => {
                        const request = handler();

                        if (!request || isBoolean(request)) {
                            done(isBoolean(request) && !request ? "failed" : "succeeded", () => handlePause(handler));
                        } else {
                            request
                                .then(() => {
                                    done("succeeded");
                                })
                                .catch((reason?: string) => {
                                    done("failed", () => handlePause(handler));

                                    if (reason) {
                                        console.log(reason);
                                    }
                                });
                        }
                    };

                    if (isFunction(props.onPause)) {
                        const pauseHandler = props.onPause;

                        handlePause(() =>
                            pauseHandler(
                                snapshot,
                                (props.l10nNamespace || L10n.Namespace.global).current,
                                (props.l10nNamespace || L10n.Namespace.global).locale.identifier,
                                namespace
                            )
                        );
                    } else if (props.onPause && props.onPause.recipe === "email") {
                        const pauseHandler = props.onPause.onPause;

                        props.onSnapshotStart();

                        pauseRef.current = {
                            recipe: "email",
                            isCompleting: false,
                            complete: (emailAddress: string) => {
                                pauseRef.current = { ...pauseRef.current!, isCompleting: true };

                                if (!snapshot.b) {
                                    set(snapshot, "b", {
                                        d: emailAddress,
                                    });
                                } else {
                                    set(snapshot.b, "d", emailAddress);
                                }

                                snapshotRef.current = snapshot;

                                runner.update();

                                handlePause(() =>
                                    pauseHandler(
                                        emailAddress,
                                        snapshot,
                                        (props.l10nNamespace || L10n.Namespace.global).current,
                                        (props.l10nNamespace || L10n.Namespace.global).locale.identifier,
                                        namespace
                                    )
                                );
                            },
                            cancel: () => done("canceled"),
                            get emailAddress() {
                                const previousSnapshot = snapshotRef.current || props.snapshot;
                                const emailBlock =
                                    runner.storyline &&
                                    findLast(runner.storyline.nodes, (node) =>
                                        node.isPassed && node.block instanceof EmailBlock ? true : false
                                    );

                                return (
                                    (previousSnapshot && previousSnapshot.b && previousSnapshot.b.d) ||
                                    (emailBlock &&
                                        emailBlock.block instanceof EmailBlock &&
                                        emailBlock.block.emailSlot.isSealed &&
                                        emailBlock.block.emailSlot.value) ||
                                    ""
                                );
                            },
                        };
                    } else {
                        done("succeeded");
                    }
                }
            );
        }

        return runner.pause(props.onSnapshot("pause"));
    };

    const controller = {
        title: runner.definition.name,
        view: runner.view,
        preview: runner.preview,
        storyline: runner.storyline,
        update: runner.update,
        start: () => {
            if (props.onStart) {
                props.onStart();
            }

            return runner.start();
        },
        restart: runner.restart,
        reload: runner.reload,
        isFinishable: (runner.storyline && (runner.storyline.isFinishable || runner.view === "preview")) || false,
        isPausable: runner.view === "live" && props.onPause ? true : false,
        pause: (runner.allowPause && props.onPause && pause) || undefined,
        pausing: pauseRef.current,
        status: runner.status,
        prologue: ((runner.status === "stopped" || runner.preview === "prologue") && runner.prologue) || undefined,
        epilogue: runner.status === "finished" || runner.preview === "epilogue" ? runner.epilogue || {} : undefined,
        discard: runner.discard,
        resetPreview: runner.resetPreview,
    };

    onRestartRef.current = props.onRestart;

    if (props.controller || props.onController) {
        controllerRef.current = {
            get definition() {
                return runner.definition;
            },
            set definition(definition: IDefinition) {
                runner.definition = definition;

                updateL10n(runner.definition && runner.definition.language);
            },
            get instance() {
                return runner.instance;
            },
            get fingerprint() {
                return runner.fingerprint;
            },
            get styles() {
                return styles;
            },
            set styles(newStyles: IAutoscrollStyles) {
                setStyles(newStyles);
            },
            get l10n() {
                return l10n;
            },
            set l10n(newL10n: TL10n) {
                setL10n(newL10n);
            },
            get view() {
                return runner.view;
            },
            set view(newView: TRunnerViews) {
                runner.view = newView;
            },
            get snapshot() {
                return runner.snapshot(props.onSnapshot("snapshot"));
            },
            get isRunning() {
                return runner.isRunning;
            },
            get isFinishing() {
                return runner.isFinishing;
            },
            get isPausing() {
                return runner.isPausing;
            },
            get isLicensed() {
                return hasLicense;
            },
            get allowStart() {
                return runner.allowStart;
            },
            get allowRestart() {
                return runner.allowRestart;
            },
            get allowPause() {
                return runner.allowPause;
            },
            get allowStop() {
                return runner.allowStop;
            },
            start: controller.start,
            restart: controller.restart,
            pause,
            stop: runner.stop,
            doPreview: runner.doPreview,
        };

        if (props.controller) {
            props.controller.current = controllerRef.current;
        }

        if (props.onController) {
            props.onController(controllerRef as MutableRefObject<IAutoscrollController>);
        }
    }

    useEffect(() => {
        if (previewRef.current && runner.view === "preview") {
            scheduleAction(() => {
                if (
                    previewRef.current &&
                    (runner.view !== "preview" || !props.onScrollIntoView || props.onScrollIntoView(previewRef.current))
                ) {
                    previewRef.current = undefined;
                }
            });
        }
    });

    return [controller, cache, runner.l10n, runtimeStyles, props.attachments, runner.doAction] as [
        typeof controller,
        typeof cache,
        typeof runner.l10n,
        typeof runtimeStyles,
        typeof props.attachments,
        typeof runner.doAction,
    ];
};
