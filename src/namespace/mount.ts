import { mountNamespace } from "@tripetto/runner";
import { namespace } from "./";

mountNamespace(namespace);
