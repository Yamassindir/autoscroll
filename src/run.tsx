import * as ReactDOM from "react-dom";
import { createRoot } from "react-dom/client";
import { useEffect, useRef, useState } from "react";
import { IDefinition, IHookPayload, ISnapshot, L10n, TL10n, compare, fingerprint, isFunction, isPromise } from "@tripetto/runner";
import { TRunnerPreviewData, TRunnerViews } from "@tripetto/runner-react-hook";
import { IAutoscrollController } from "@hooks/controller";
import { IAutoscroll } from "@interfaces/autoscroll";
import { IAutoscrollProps } from "@interfaces/props";
import { IAutoscrollStyles } from "@interfaces/styles";
import { IAutoscrollRunner } from "@interfaces/runner";
import { IAutoscrollSnapshot } from "@interfaces/snapshot";
import { IBuilderInstance } from "@interfaces/builder";
import { AutoscrollUI } from "./autoscroll";
import { namespace } from "@namespace";

declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** This counter is incremented on each run. */
let runCounter = 0;

export const AutoscrollRunner = (props: IAutoscrollProps) => {
    const definition = useRef<IDefinition | false>();
    const snapshot = useRef<ISnapshot<IAutoscrollSnapshot> | false>();
    const styles = useRef<IAutoscrollStyles | false>();
    const l10n = useRef<TL10n | false>();
    const license = useRef<string | false>();
    const emptyDefinition = {
        sections: [],
        builder: {
            name: "",
            version: "",
        },
    } as IDefinition;
    const isReady = useRef<true>();
    const runOnce = useRef<true>();
    const builderRef = useRef<IBuilderInstance>();
    const builderAwait = useRef<boolean>();
    const isLive = props.view !== "test" && props.view !== "preview";
    const controllerInternal = useRef<IAutoscrollController>();
    const controller = props.controller || controllerInternal;
    const [, updateProc] = useState({});
    const componentState = useRef<"mounted" | "dirty">();
    const updateRef = useRef<typeof updateProc>();
    const update = (apply: () => void) => {
        apply();

        if (componentState.current === "mounted") {
            updateRef.current!({});
        } else {
            componentState.current = "dirty";
        }
    };

    updateRef.current = updateProc;

    if (!runOnce.current) {
        runOnce.current = true;
        builderAwait.current = props.builder ? true : false;

        if (isPromise(props.definition)) {
            definition.current = false;

            props.definition
                .then((data) => update(() => (definition.current = data || emptyDefinition)))
                ?.catch(() => update(() => (definition.current = emptyDefinition)));
        } else {
            definition.current = props.definition || emptyDefinition;
        }

        if (isPromise(props.snapshot)) {
            snapshot.current = false;

            props.snapshot
                .then((data) => update(() => (snapshot.current = data)))
                ?.catch(() => update(() => (snapshot.current = undefined)));
        } else {
            snapshot.current = props.snapshot || undefined;
        }

        if (isPromise(props.styles)) {
            styles.current = false;

            props.styles.then((data) => update(() => (styles.current = data)))?.catch(() => update(() => (styles.current = undefined)));
        } else {
            styles.current = props.styles || undefined;
        }

        if (isPromise(props.l10n)) {
            l10n.current = false;

            props.l10n.then((data) => update(() => (l10n.current = data)))?.catch(() => update(() => (l10n.current = undefined)));
        } else {
            l10n.current = props.l10n || undefined;
        }

        if (isPromise(props.license)) {
            license.current = false;

            props.license.then((data) => update(() => (license.current = data)))?.catch(() => update(() => (license.current = undefined)));
        } else {
            license.current = props.license || undefined;
        }
    }

    const l10nNamespace = useRef<L10n.Namespace>();
    const l10nCache = useRef<{
        locales: {
            [domain: string]: {
                locale: L10n.ILocale | undefined;
            };
        };
        translations: {
            [domain: string]: {
                translation: L10n.TTranslation | L10n.TTranslation[] | undefined;
            };
        };
    }>({
        locales: {},
        translations: {},
    });

    const processL10n = async (data: TL10n) => {
        const currentDefinition = controller.current?.definition || definition.current;

        l10nNamespace.current!.reset(
            (data.language !== "auto" && data.language) ||
                (currentDefinition && currentDefinition.language) ||
                props.language ||
                navigator.language
        );

        if (props.translations || props.locale) {
            const localeDomain = data.locale || "auto";
            const localeResult = l10nCache.current.locales[localeDomain]
                ? l10nCache.current.locales[localeDomain].locale
                : isFunction(props.locale)
                ? props.locale(localeDomain)
                : props.locale;
            const translationResult = l10nCache.current.translations[l10nNamespace.current!.current]
                ? l10nCache.current.translations[l10nNamespace.current!.current].translation
                : (isFunction(props.translations)
                      ? props.translations(l10nNamespace.current!.current, PACKAGE_NAME, PACKAGE_VERSION)
                      : props.translations) || undefined;
            const [locale, translation] = await Promise.all([Promise.resolve(localeResult), Promise.resolve(translationResult)]);

            l10nCache.current.locales[localeDomain] = {
                locale,
            };

            l10nCache.current.translations[l10nNamespace.current!.current] = {
                translation,
            };

            if (locale) {
                l10nNamespace.current!.locale.load(locale);
            }

            if (translation) {
                l10nNamespace.current!.load(translation, false);
            }
        }

        if (data.translations) {
            l10nNamespace.current!.load(data.translations, false, "overwrite");
        }
    };

    const localSnapshot = useRef({
        data: undefined as ISnapshot<IAutoscrollSnapshot> | undefined,
        save: () => {
            if (controller.current && localStorage) {
                const key = `${PACKAGE_NAME}-${controller.current.fingerprint}`;
                const data = controller.current.snapshot;

                if (data) {
                    localStorage.setItem(key, JSON.stringify(data));
                } else {
                    localStorage.removeItem(key);
                }
            }
        },
    });

    if (!l10nNamespace.current && definition.current !== false && l10n.current !== false) {
        l10nNamespace.current = L10n.Namespace.create(`${namespace}:${runCounter++}`);

        processL10n(l10n.current || {})
            .then(() => update(() => (isReady.current = true)))
            .catch(() => update(() => (isReady.current = true)));
    }

    if (props.persistent && isLive && definition.current !== false && typeof window !== "undefined" && localStorage) {
        localSnapshot.current.data =
            JSON.parse(localStorage.getItem(`${PACKAGE_NAME}-${fingerprint(definition.current || emptyDefinition)}`) || "null") ||
            undefined;
    }

    useEffect(() => {
        const isDirty = componentState.current === "dirty";
        const allowListener = props.persistent && isLive && typeof window !== "undefined" && localStorage;
        let handle = 0;

        componentState.current = "mounted";

        if (allowListener) {
            window.addEventListener("unload", localSnapshot.current.save);
        }

        if (props.builder && !builderRef.current) {
            new Promise((resolve: (builder: IBuilderInstance) => void) => {
                const fnAwait = () => {
                    handle = 0;

                    if (props.builder?.current) {
                        resolve(props.builder?.current);
                    } else {
                        handle = requestAnimationFrame(fnAwait);
                    }
                };

                fnAwait();
            }).then((builder) => {
                if (!builderRef.current) {
                    builderRef.current = builder;

                    update(() => {
                        definition.current = builder.definition;
                        builderAwait.current = false;
                    });

                    builder.hook<
                        "OnChange",
                        IHookPayload<"OnChange"> & {
                            readonly definition: IDefinition;
                        }
                    >("OnChange", "synchronous", (changeEvent) => {
                        if (controller.current) {
                            controller.current.definition = changeEvent.definition;
                        }
                    });

                    builder.hook<"OnEdit", IHookPayload<"OnEdit"> & { readonly data: TRunnerPreviewData }>(
                        "OnEdit",
                        "synchronous",
                        (editEvent) => {
                            controller.current?.doPreview(editEvent.data);
                        }
                    );
                }
            });
        }

        if (isDirty) {
            updateProc({});
        }

        return () => {
            if (allowListener) {
                window.removeEventListener("unload", localSnapshot.current.save);
            }

            if (handle !== 0) {
                cancelAnimationFrame(handle);
            }
        };
    });

    if (controller.current) {
        if (props.view) {
            controller.current.view = props.view;
        }

        if (!isPromise(props.definition) && props.definition && !compare(props.definition, controller.current.definition, true)) {
            controller.current.definition = definition.current = props.definition;
        }

        if (!isPromise(props.styles) && props.styles && !compare(props.styles, controller.current.styles, true)) {
            controller.current.styles = styles.current = props.styles;
        }

        if (!isPromise(props.l10n) && props.l10n && !compare(props.l10n, controller.current.l10n, true)) {
            controller.current.l10n = l10n.current = props.l10n;
        }
    }

    return (
        <>
            {!isReady.current ||
            definition.current === false ||
            snapshot.current === false ||
            styles.current === false ||
            license.current === false ||
            l10n.current === false ||
            builderAwait.current ? (
                props.loader
            ) : (
                <AutoscrollUI
                    {...props}
                    controller={controller}
                    definition={definition.current || emptyDefinition}
                    snapshot={snapshot.current || localSnapshot.current.data}
                    styles={styles.current}
                    license={license.current}
                    l10nNamespace={l10nNamespace.current}
                    l10n={l10n.current}
                    onL10n={processL10n}
                    onEdit={
                        props.builder
                            ? (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => {
                                  if (props.builder?.current) {
                                      switch (type) {
                                          case "prologue":
                                              props.builder.current.edit("prologue");
                                              break;
                                          case "epilogue":
                                              props.builder.current.edit("epilogue", id);
                                              break;
                                          case "block":
                                              if (id) {
                                                  props.builder.current.edit("node", id);
                                              }
                                              break;
                                      }
                                  }

                                  if (props.onEdit) {
                                      props.onEdit(type, id);
                                  }
                              }
                            : props.onEdit
                    }
                />
            )}
        </>
    );
};

/**
 * Bootstraps a new runner.
 * @param props Specifies the properties for the runner.
 * @returns Returns a promise to the runner controller.
 */
export const run = (props: IAutoscroll) => {
    return new Promise((resolve: (ref: IAutoscrollRunner) => void) => {
        const element = props.element || document.body.appendChild(document.createElement("div"));
        const render =
            typeof createRoot === "function"
                ? (jsx: JSX.Element) => {
                      const root = createRoot(element);

                      root.render(jsx);

                      return () => root.unmount();
                  }
                : (jsx: JSX.Element) => {
                      ReactDOM.render(jsx, element);

                      return () => ReactDOM.unmountComponentAtNode(element);
                  };

        const unmount = render(
            <AutoscrollRunner
                {...props}
                display={props.display || (document.body.isEqualNode(element) && "page") || undefined}
                builder={
                    (props.builder && {
                        current: props.builder,
                    }) ||
                    undefined
                }
                onController={(controller) => {
                    resolve({
                        get definition() {
                            return controller.current.definition;
                        },
                        set definition(newDefinition: IDefinition) {
                            controller.current.definition = newDefinition;
                        },
                        get instance() {
                            return controller.current.instance;
                        },
                        get fingerprint() {
                            return controller.current.fingerprint;
                        },
                        get styles() {
                            return controller.current.styles;
                        },
                        set styles(newStyles: IAutoscrollStyles) {
                            controller.current.styles = newStyles;
                        },
                        get l10n() {
                            return controller.current.l10n;
                        },
                        set l10n(newL10n: TL10n) {
                            controller.current.l10n = newL10n;
                        },
                        get view() {
                            return controller.current.view;
                        },
                        set view(newView: TRunnerViews) {
                            controller.current.view = newView;
                        },
                        get snapshot() {
                            return controller.current.snapshot;
                        },
                        get isRunning() {
                            return controller.current.isRunning;
                        },
                        get isFinishing() {
                            return controller.current.isFinishing;
                        },
                        get isPausing() {
                            return controller.current.isPausing;
                        },
                        get isLicensed() {
                            return controller.current.isLicensed;
                        },
                        get allowStart() {
                            return controller.current.allowStart;
                        },
                        get allowRestart() {
                            return controller.current.allowRestart;
                        },
                        get allowPause() {
                            return controller.current.allowPause;
                        },
                        get allowStop() {
                            return controller.current.allowStop;
                        },
                        start: () => {
                            return controller.current.start();
                        },
                        restart: () => {
                            return controller.current.restart();
                        },
                        pause: () => {
                            return controller.current.pause();
                        },
                        stop: () => {
                            return controller.current.stop();
                        },
                        doPreview: (data: TRunnerPreviewData) => {
                            return controller.current.doPreview(data);
                        },
                        destroy: () => {
                            unmount();
                        },
                    });
                }}
            />
        );
    });
};
