import Superagent from "superagent";
import { createRoot } from "react-dom/client";
import { createRef } from "react";
import { Builder, IBuilderChangeEvent, IBuilderReadyEvent, IDefinition, pgettext } from "@tripetto/builder";
import { Export, ISnapshot, TL10n } from "@tripetto/runner";
import { IAutoscrollSnapshot, IAutoscrollStyles, run } from "../..";
import { Header } from "./header/header";
import STYLES_CONTRACT from "../../styles";
import L10N_CONTRACT from "../../../builder/l10n";
import "../../builder";

declare const PACKAGE_NAME: string;

const DEFINITION = PACKAGE_NAME + ":definition";
const SNAPSHOT = PACKAGE_NAME + ":snapshot";
const STYLES = PACKAGE_NAME + ":styles";
const L10N = PACKAGE_NAME + ":l10n";

// For this demo app we use the local store to save the definition, snapshot, styles and l10n settings.
// Here we try to retrieve that saved data.
const definition: IDefinition | undefined = JSON.parse(localStorage.getItem(DEFINITION) || "null") || undefined;
const snapshot: ISnapshot<IAutoscrollSnapshot> | undefined = JSON.parse(localStorage.getItem(SNAPSHOT) || "null") || undefined;
const styles: IAutoscrollStyles | undefined = JSON.parse(localStorage.getItem(STYLES) || "null") || undefined;
const l10n: TL10n | undefined = JSON.parse(localStorage.getItem(L10N) || "null") || undefined;
const license =
    "ogFfKAAYE2mSewYyatDuH9AN870U0QAPT6HPnug0WjoTxgyuMZbVXrEO7O1oMoZvIDv24AGq1Cx21yDqAIGGbVGlVGz0YmM/TMmVX7YxEtm+sKxw8jV2l8e2n5NATFjRYcxbzpw1XyEyB8DpFuWuchNYfuv6FN42NFnMwYPUL5KOzmGxu/J6fS3Pga4v2FlMEVtoLhLX0fNaBNA/KwYQjz7xVgzochFp7XI3PNLkBfVN9bkjWUnb7zJ6XHxeBg0FCkVIIepbSMTvTUdcckFthI+MOuohpwFQ9oXdnTPmklrMgl08Y8mTZc5cnSAYa7EVbCdzgYTtr/fHFt2O1RGebL6nKgra57hSo+/+efhZ0mUoWgtb9Okogbcgrvvq5hft80OC6nagRZUmvkMHrzhQHw06pO5bos3/AfSBbuyv4H3NbxEK4U+u+fAwD2lyFxLevw8LkgC+JJ0nNj3hRVQA9nWVxq/c9HZklm8WCQx31baN4+82CuiSqS41h0VUhb6FLjKD/c9c6xBBFw4RYCJeYCJKi8tXUav5GDLWdXeUrAcRPHbc9thTbP/h+5cQKl/32ceinFJwq+1rjCDNtQ4pZjd0GBW3LSbvZ+PekuQI0SXFGoTe3iMlbWrkYqUEPc6RLZ5j16c59eytGV7g3II4wP22aoEsr0PIMebM1+e9lTYO0B296xOSOo895hINC+yEl4Ll5K6tFf8mribQXJ+MJSoT6BcjeFz9XbCt59Jx9iBaDoJF2QRPyb+sNe6K5DoG9kJgKTTNqH85B1gbxBlDtfmnw/CfbVeQdrKiv5OZZzWH0Zx7EcCO5XexJZJ79OcjcSWh9On1ZkUrZ32C+9Ffpy25ZNbYh3wBVt6mMC6GG/HG3T2RYiz3s21I17oa8N8+KtIg8paHtT5zZx121F2XZ9H+70xBBxM2xQqKUQ5t6Q3FCJksLuZ8S2b0HLt0x3TyrUil/OXT3wS62M4RFpAUPodys9qa16vkyg4sBDRBsk29x6mYHfVXGM8idWf3s0tUKZBKoixFPsSUcgP+HB+Qso4sClGi76mlNsMA9X5lhnloUdeqWijnd3IP6ctEGiWemj46Ajb4NJkuxmytuJhq/vdjPZyIkBQe7VGVz2Q=";

// Create a builder instance.
const builder = Builder.open(definition, {
    element: document.getElementById("builder"),
    fonts: "fonts/",
    disableSaveButton: true,
    disableRestoreButton: true,
    disableClearButton: true,
    disableCloseButton: true,
    disableOpenCloseAnimation: true,
    disableBookmarks: true,
    zoom: "fit-horizontal",
    license,
});

// Wait until the builder is ready!
builder.hook("OnReady", "synchronous", async (builderEvent: IBuilderReadyEvent) => {
    const updateRef = createRef<(() => void) | undefined>();
    const runner = await run({
        element: document.getElementById("runner"),
        definition: builderEvent.definition,
        view: "test",
        builder,
        snapshot,
        styles,
        l10n,
        license,
        /*
        onImport: (i) => {
            Import.fields(i, [
                {
                    name: "NAME",
                    value: "Test",
                },
            ]);
        },
        */
        onChange: () => {
            if (updateRef.current) {
                updateRef.current();
            }
        },
        onAction: (t, f, b) => {
            console.log(t);
            console.log(f);
            console.log(b);
        },
        onSubmit: (instance, language, locale, namespace) =>
            new Promise((resolve: (reference: string) => void /*, reject: (reason?: string) => void*/) => {
                console.log(`Form submitted using runner ${namespace}!`);
                console.log(`Language: ${language} / Locale: ${locale}`);

                // Output the node props
                /*
                Export.exportables(instance).fields.forEach((field) => {
                    console.log(field.node.props());
                });
                */

                // Output the collected data to the console for demo purposes.
                console.dir(Export.exportables(instance));

                // Output can also be exported as CSV for your convenience.
                console.dir(Export.CSV(instance));

                setTimeout(() => {
                    resolve("DEMO123");
                }, 1000);

                /*
                setTimeout(() => reject("outdated"), 2000);
                setTimeout(() => reject("rejected"), 2000);
                setTimeout(() => reject("Simulating a connection error."), 2000);
                */
            }),
        onReload: () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                if (definition) {
                    setTimeout(() => resolve(definition), 1000);
                } else {
                    reject();
                }
            }),
        onComplete: (instance, id) => {
            console.log(`OnComplete: ${id}`);
            console.dir(Export.exportables(instance));
            console.dir(Export.NVPs(instance));
            console.dir(Export.NVPs(instance, "strings"));
        },
        onPause: {
            recipe: "email",
            onPause: (emailAddress, snapshotData, language, locale, namespace) =>
                new Promise((resolve: () => void /*, reject: (reason?: string) => void*/) => {
                    console.log(`Pausing using ${namespace} to ${emailAddress}`);
                    console.log(`Language: ${language} / Locale: ${locale}`);
                    console.dir(snapshotData);

                    try {
                        localStorage.setItem(SNAPSHOT, JSON.stringify(snapshotData));
                    } catch {
                        console.log("Error while saving snapshot to localStorage.");
                    }

                    setTimeout(() => resolve(), 2000);
                    /*
                    setTimeout(() => reject("Simulating a connection error."), 2000);
                    */
                }),
        },
        /*
        onPause: (snapshotData) => {
            console.dir(snapshotData);
        },
        */
        onEdit: (type) => {
            switch (type) {
                case "styles":
                    builder.stylesEditor(
                        STYLES_CONTRACT(pgettext),
                        runner.styles,
                        runner.isLicensed ? "licensed" : "unlicensed",
                        (newStyles) => {
                            localStorage.setItem(STYLES, JSON.stringify((runner.styles = newStyles)));

                            console.dir(newStyles);
                        }
                    );
                    break;
                case "l10n":
                    builder.l10nEditor(L10N_CONTRACT(), runner.l10n, (newL10n) => {
                        localStorage.setItem(L10N, JSON.stringify((runner.l10n = newL10n)));

                        console.dir(newL10n);
                    });
                    break;
            }
        },
    });

    createRoot(document.getElementById("header")!).render(
        <Header
            builder={builder}
            runner={runner}
            updateRef={updateRef}
            editStyles={() => {
                builder.stylesEditor(
                    STYLES_CONTRACT(pgettext),
                    runner.styles,
                    runner.isLicensed ? "licensed" : "unlicensed",
                    (newStyles) => {
                        localStorage.setItem(STYLES, JSON.stringify((runner.styles = newStyles)));

                        console.dir(newStyles);
                    }
                );
            }}
            editL10n={() => {
                builder.l10nEditor(L10N_CONTRACT(), runner.l10n, (newL10n) => {
                    localStorage.setItem(L10N, JSON.stringify((runner.l10n = newL10n)));

                    console.dir(newL10n);
                });
            }}
            onLoadExample={() => {
                Superagent.get("example.json").end((error, response) => {
                    if (response?.ok) {
                        builder.definition = JSON.parse(response.text);
                    }
                });
            }}
            onClear={() => {
                builder.clear();
            }}
        />
    );

    // Store the definition in the local store upon each builder change and reload the runner
    builder.hook("OnChange", "synchronous", (changeEvent: IBuilderChangeEvent) => {
        try {
            localStorage.setItem(DEFINITION, JSON.stringify(changeEvent.definition));
            localStorage.removeItem(SNAPSHOT);
        } catch {
            console.log("Error while saving definition to localStorage.");
        }
    });
});
