import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { PhoneNumber } from "@tripetto/block-phone-number/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { PhoneNumberFabric } from "@tripetto/runner-fabric/components/phone-number";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-phone-number",
})
export class PhoneNumberBlock extends PhoneNumber implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <PhoneNumberFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.phoneNumberSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
