import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Checkboxes } from "@tripetto/block-checkboxes/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { CheckboxesFabric } from "@tripetto/runner-fabric/components/checkboxes";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-checkboxes",
})
export class CheckboxesBlock extends Checkboxes implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <CheckboxesFabric
                    styles={props.styles.checkboxes}
                    view={props.view}
                    checkboxes={this.checkboxes(props)}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
