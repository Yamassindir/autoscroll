import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Ranking } from "@tripetto/block-ranking/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { RankingFabric } from "@tripetto/runner-fabric/components/ranking";

@tripetto({
    namespace,
    type: "node",
    identifier: "@tripetto/block-ranking",
})
export class RankingBlock extends Ranking implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <RankingFabric
                    styles={props.styles.multipleChoice}
                    slots={this.slots}
                    options={this.options(props)}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
            </>
        );
    }
}
