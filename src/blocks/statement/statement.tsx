import { styled } from "styled-components";
import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Statement } from "@tripetto/block-statement/runner";
import { IAutoscrollRenderProps, IAutoscrollRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";

const StatementElement = styled.div<{
    $color: string;
}>`
    padding-left: 2.2em;
    background-image: ${(props) =>
        `url("data:image/svg+xml;base64,${btoa(
            `<?xml version="1.0" encoding="utf-8"?>
            <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 10240 10240">
            <path d="M6400 8320l1920 0c353,0 640,-287 640,-640l0 -1920c0,-353 -287,-640 -640,-640l-960 0c0,-499 0,-1464 1310,-1587 165,-16 290,-153 290,-319l0 -966c0,-91 -34,-169 -100,-232 -66,-63 -145,-92 -236,-88 -2864,146 -2864,2164 -2864,3192l0 2560c0,353 287,640 640,640z" fill="${props.$color}" />
            <path d="M1920 8320l1920 0c353,0 640,-287 640,-640l0 -1920c0,-353 -287,-640 -640,-640l-960 0c0,-499 0,-1464 1310,-1587 165,-16 290,-153 290,-319l0 -966c0,-91 -34,-169 -100,-232 -66,-63 -145,-92 -236,-88 -2864,146 -2864,2164 -2864,3192l0 2560c0,353 287,640 640,640z" fill="${props.$color}" />
        </svg>`
        )}")`};
    background-repeat: no-repeat;
    background-size: 1.6em 1.6em;
    background-position: top left;

    > h2 {
        font-weight: normal;
    }
`;

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-statement",
    alias: "statement",
})
export class StatementBlock extends Statement implements IAutoscrollRendering {
    readonly autoFocus = true;

    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            <StatementElement $color={props.styles.font.color}>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
            </StatementElement>
        );
    }
}
