import { each, findFirst } from "@tripetto/runner";

interface IRef {
    readonly update: (force: boolean) => void;
    readonly index: number;
}

const refs: {
    [index: number]: IRef | undefined;
} = {};

const element: {
    [id: string]: number | undefined;
} = {};

const state: {
    [id: string]: boolean | undefined;
} = {};

let index = 0;

export const attach = (update: (force: boolean) => void): IRef => {
    while (refs[index]) {
        index++;
    }

    update(false);

    return (refs[index] = {
        update,
        index,
    });
};

export const detach = (ref: IRef): void => {
    index = ref.index;

    delete refs[ref.index];
};

export const useLoader = (id: string) => {
    const fnUpdate = () => {
        if (!isLoading()) {
            delete element[id];

            each(refs, (ref: IRef | undefined) => {
                if (ref) {
                    ref.update(true);
                }
            });
        }
    };

    if (typeof element[id] === "number") {
        element[id]!++;
    } else {
        element[id] = 1;
    }

    if (typeof state[id] !== "boolean") {
        state[id] = false;
    }

    return [
        () => {
            if (!state[id]) {
                state[id] = true;

                fnUpdate();
            }
        },
        () => {
            if (typeof element[id] === "number" && element[id]! > 0) {
                element[id]!--;

                if (!element[id]) {
                    delete element[id];
                    delete state[id];
                }
            }
        },
    ];
};

export const isLoading = () => typeof findFirst(state, (source) => !source) === "boolean";
