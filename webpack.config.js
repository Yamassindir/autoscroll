const webpack = require("webpack");
const webpackTerser = require("terser-webpack-plugin");
const webpackShell = require("webpack-shell-plugin-next");
const webpackCopy = require("copy-webpack-plugin");
const path = require("path");
const banner = require("./tasks/banner/banner.js");
const pkg = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const config = (type, target, production) => {
    const externals =
        type !== "app"
            ? {
                  "@tripetto/builder": target === "umd" ? "Tripetto" : "commonjs @tripetto/builder",
                  "@tripetto/runner": target === "umd" ? "TripettoRunner" : "commonjs @tripetto/runner",
                  ...(target !== "umd"
                      ? {
                            react: "commonjs react",
                            "react-dom": "commonjs react-dom",
                            "styled-components": "commonjs styled-components",
                        }
                      : {}),
              }
            : {};
    let entry;
    let output;

    switch (type) {
        case "runner":
            entry = "./src/index.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "runner", (target !== "umd" && target) || ""),
                library:
                    target === "umd"
                        ? {
                              name: "TripettoAutoscroll",
                              type: "umd",
                              umdNamedDefine: true,
                          }
                        : {
                              type: "commonjs2",
                          },
                globalObject: (target === "umd" && "this") || undefined,
            };
            break;
        case "builder":
            entry = "./src/builder/index.ts";
            output = {
                filename: "index.js",
                path: path.resolve(__dirname, "builder", (target !== "umd" && target) || ""),
                library:
                    target === "umd"
                        ? {
                              name: "TripettoAutoscrollBuilder",
                              type: "umd",
                              umdNamedDefine: true,
                          }
                        : {
                              type: "commonjs2",
                          },
            };
            break;
        default:
            entry = "./src/tests/app/app.tsx";
            output = {
                filename: "bundle.js",
                path: path.resolve(__dirname, "src/tests/app/static"),
            };
            break;
    }

    return {
        target: ["web", "es5"],
        entry,
        output,
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    options: {
                        compilerOptions: {
                            noEmit: false,
                            target: "ES5",
                            module: "CommonJS",
                            jsx: production ? "react-jsx" : "react-jsxdev",
                        },
                    },
                },
            ],
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js"],
            alias: {
                "@namespace": path.resolve(__dirname, "./src/namespace"),
                "@blocks": path.resolve(__dirname, "./src/blocks"),
                "@helpers": path.resolve(__dirname, "./src/helpers"),
                "@hooks": path.resolve(__dirname, "./src/hooks"),
                "@interfaces": path.resolve(__dirname, "./src/interfaces"),
                "@l10n": path.resolve(__dirname, "./builder/l10n"),
                "@styles": path.resolve(__dirname, "./src/styles"),
                "@ui": path.resolve(__dirname, "./src/ui"),
            },
        },
        externals,
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        format: {
                            preamble: `/*! ${banner} */`,
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                PACKAGE_NAME: JSON.stringify(pkg.name),
                PACKAGE_TITLE: JSON.stringify(pkg.title),
                PACKAGE_VERSION: JSON.stringify(pkg.version),
            }),
            new analyzer({
                analyzerMode: "static",
                reportFilename: `../${!target ? "../../../" : target !== "umd" ? "../" : ""}reports/bundle-${
                    type === "app" ? type : `${type}-${target}`
                }.html`,
                openAnalyzer: false,
            }),
            ...(type === "runner" && target === "umd"
                ? [
                      new webpackCopy({
                          patterns: [{ from: "translations/template.pot", to: "translations/" }],
                      }),
                  ]
                : []),
            ...(type === "app"
                ? [
                      new webpackShell({
                          onBuildStart: {
                              scripts: ["npm run make:l10n"],
                              blocking: true,
                              parallel: false,
                          },
                      }),
                      new webpackCopy({
                          patterns: [{ from: "node_modules/@tripetto/builder/fonts/", to: "fonts/" }],
                      }),
                  ]
                : []),
        ],
        devServer: {
            static: path.resolve(__dirname, "src/tests/app/static"),
            port: 9000,
            host: "0.0.0.0",
        },
    };
};

module.exports = production
    ? [
          config("builder", "umd", true),
          config("builder", "es5", true),
          config("runner", "umd", true),
          config("runner", "es5", true),
          config("app", undefined, true),
      ]
    : [config("app")];
