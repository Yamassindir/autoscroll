const fs = require("fs");
const po2json = require("po2json");
const prettier = require("prettier");
const tripetto = require("@tripetto/builder");
const pkg = require("../../package.json");

const inputFolder = "./translations/";
const outputFolder = "./builder/l10n/";
const filter = /^runner(\||(#[0-9]+\|))/;
const files = (fs.existsSync(inputFolder) && fs.readdirSync(inputFolder)) || [];
const contract = {
    contract: {
        name: pkg.name,
        version: pkg.version,
    },
    customLocale: true,
    availableTranslations: ["en"],
};

fs.mkdirSync(outputFolder, { recursive: true });

files.forEach(function (file) {
    if (fs.statSync(inputFolder + file).isFile()) {
        const isPO = file.lastIndexOf(".po") === file.length - 3;

        if (isPO) {
            const domain = file.substr(0, file.length - 3);

            if (domain !== "") {
                contract["availableTranslations"] = contract["availableTranslations"] || [];
                contract["availableTranslations"].push(domain);

                console.log(`l10n: Found language '${domain}'`);
            }
        }
    }
});

if (fs.existsSync(inputFolder + "template.pot")) {
    const pot = po2json.parseFileSync(inputFolder + "template.pot");

    tripetto.each(
        pot,
        (data, id) => {
            if (filter.test(id)) {
                contract["strings"] = contract["strings"] || [];
                contract["strings"].push(data.length > 2 && tripetto.isString(data[0]) ? [id, data[0]] : id);
            }
        },
        {
            keys: true,
        }
    );

    if (contract["strings"]) {
        contract["strings"].sort();
    }
}

async function write() {
    const count = (contract["strings"] && contract["strings"].length) || 0;

    console.log(`l10n: Parsed ${count} string${count === 1 ? "" : "s"}`);

    fs.writeFileSync(
        outputFolder + "index.js",
        await prettier.format(
            `
            "use strict";
            Object.defineProperty(exports, "__esModule", { value: true });
            exports.default = function() {
                return ${JSON.stringify(contract)};
            }
        `,
            {
                parser: "babel",
            }
        ),
        "utf8"
    );

    fs.writeFileSync(
        outputFolder + "index.mjs",
        await prettier.format(
            `
        export default () => (${JSON.stringify(contract)});
        `,
            {
                parser: "babel",
            }
        ),
        "utf8"
    );

    fs.writeFileSync(
        outputFolder + "index.d.ts",
        await prettier.format(
            `
            import { TL10nContract } from "@tripetto/runner";
            declare const _default: () => TL10nContract;
            export default _default;
        `,
            {
                parser: "typescript",
            }
        ),
        "utf8"
    );
}

write();
