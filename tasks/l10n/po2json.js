const fs = require("fs");
const path = require("path");
const po2json = require("po2json");
const prettier = require("prettier");

function convert(inputFolder, outputFolder) {
    if (fs.existsSync(inputFolder)) {
        const files = fs.readdirSync(inputFolder) || [];

        fs.mkdirSync(outputFolder, { recursive: true });

        files.forEach(async function (file) {
            if (fs.statSync(inputFolder + file).isFile()) {
                const isPO = file.lastIndexOf(".po") === file.length - 3;

                if (isPO) {
                    fs.writeFileSync(
                        outputFolder + path.basename(file, ".po") + ".json",
                        await prettier.format(JSON.stringify(po2json.parseFileSync(inputFolder + file)), {
                            parser: "json",
                        }),
                        "utf8"
                    );

                    console.log(`po2json: ${inputFolder + file} -> ${outputFolder + path.basename(file, ".po") + ".json"}`);
                }
            }
        });
    }
}

convert("./translations/", "./runner/translations/");
