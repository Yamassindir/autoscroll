const fs = require("fs");
const prettier = require("prettier");
const tripetto = require("@tripetto/builder");
const pkg = require("../../package.json");

function combine(inputFolder, outputFolder) {
    if (fs.existsSync(inputFolder)) {
        const files = fs.readdirSync(inputFolder) || [];

        fs.mkdirSync(outputFolder, { recursive: true });

        files.forEach(async function (file) {
            if (fs.statSync(inputFolder + file).isFile()) {
                const isJSON = file.lastIndexOf(".json") === file.length - 5;

                if (isJSON) {
                    const inputData = JSON.parse(fs.readFileSync(inputFolder + file, "utf8"));

                    if (inputData && (inputData[""] || (inputData.length > 0 && inputData[0][""]))) {
                        if (fs.existsSync(outputFolder + file)) {
                            let outputData = JSON.parse(fs.readFileSync(outputFolder + file, "utf8"));

                            if (JSON.stringify(outputData).indexOf(JSON.stringify(inputData)) === -1) {
                                if (typeof outputData === "object" && "length" in outputData && outputData.length > 0) {
                                    outputData.push(inputData);
                                } else {
                                    outputData = [outputData, inputData];
                                }

                                fs.writeFileSync(
                                    outputFolder + file,
                                    await prettier.format(JSON.stringify(outputData), {
                                        parser: "json",
                                    }),
                                    "utf8"
                                );

                                console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (merged)`);
                            } else {
                                console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (skipped, already merged)`);
                            }
                        } else {
                            fs.writeFileSync(
                                outputFolder + file,
                                await prettier.format(JSON.stringify(inputData), {
                                    parser: "json",
                                }),
                                "utf8"
                            );

                            console.log(`combine: ${inputFolder + file} -> ${outputFolder + file} (created)`);
                        }
                    }
                }
            }
        });
    }
}

tripetto.each(
    tripetto.filter(Object.keys(pkg.devDependencies), (p) => /^\@tripetto\/block-/.test(p)),
    (p) => combine(`./node_modules/${p}/translations/`, "./builder/translations/")
);

combine(`./runner/translations/`, "./builder/translations/");
