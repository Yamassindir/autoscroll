const fs = require("fs");
const path = "./runner/types/index.d.ts";
const banner = require("../banner/banner.js");
const declaration = fs.readFileSync(path, "utf8").split("\n");
let errors = 4;

for (let row = 0; row < declaration.length; row++) {
    declaration[row] = declaration[row].replace("declare ", "");

    if (declaration[row].indexOf("private ") !== -1) {
        if (row > 0 && declaration[row - 1].indexOf("*/") !== -1) {
            let comment = row - 1;

            while (comment > 0 && declaration[comment].indexOf("/*") === -1) {
                declaration[comment] = "";

                comment--;
            }

            declaration[comment] = "";
        }

        declaration[row] = "";
    }

    if (declaration[row].indexOf("export {};") !== -1) {
        declaration[row] = "";
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { Hooks, IDefinition, ISnapshot, Instance, NodeBlock, TL10n, TStyles } from '@tripetto/runner';`
        ) !== -1
    ) {
        declaration[row] = `import { Hooks, IDefinition, ISnapshot, Instance, L10n, NodeBlock, TL10n, TStyles } from '@tripetto/runner';`;
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { IRunnerAttachments, IRunnerProps, TRunnerPreviewData, TRunnerViews } from '@tripetto/runner-react-hook';`
        ) !== -1
    ) {
        declaration[row] += `\nimport { styled } from "styled-components";\n`;
        errors--;
    }

    if (declaration[row].indexOf(`export { css, keyframes } from "styled-components";`) !== -1) {
        declaration[row] = `export { styled };\n${declaration[row]}`;
        errors--;
    }
}

if (errors !== 0) {
    throw new Error("Declaration generation failed!");
}

const header = [
    `declare module "@tripetto/runner-autoscroll" {`,
    `import * as TripettoAutoscroll from "@tripetto/runner-autoscroll/module";`,
    `export * from "@tripetto/runner-autoscroll/module";`,
    `export { TripettoAutoscroll };`,
    `export default TripettoAutoscroll;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-autoscroll/es5" {`,
    `import * as TripettoAutoscroll from "@tripetto/runner-autoscroll/module";`,
    `export * from "@tripetto/runner-autoscroll/module";`,
    `export { TripettoAutoscroll };`,
    `export default TripettoAutoscroll;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-autoscroll/runner" {`,
    `import * as TripettoAutoscroll from "@tripetto/runner-autoscroll/module";`,
    `export * from "@tripetto/runner-autoscroll/module";`,
    `export { TripettoAutoscroll };`,
    `export default TripettoAutoscroll;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-autoscroll/runner/es5" {`,
    `import * as TripettoAutoscroll from "@tripetto/runner-autoscroll/module";`,
    `export * from "@tripetto/runner-autoscroll/module";`,
    `export { TripettoAutoscroll };`,
    `export default TripettoAutoscroll;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-autoscroll/react" {`,
    `import * as TripettoAutoscroll from "@tripetto/runner-autoscroll/module";`,
    `export * from "@tripetto/runner-autoscroll/module";`,
    `export { TripettoAutoscroll };`,
    `export default TripettoAutoscroll;`,
    `}`,
];

fs.writeFileSync(
    path,
    `/*! ${banner} */\n\n` +
        `${header.join("\n")}\n\n` +
        `declare module "@tripetto/runner-autoscroll/module" {\n${declaration.join("\n")}\n}\n`,
    "utf8"
);
