import TripettoAutoscrollAsDefaultImport from "../runner/esm/index.mjs";
import { TripettoAutoscroll, AutoscrollRunner } from "../runner/esm/index.mjs";
import "../builder/esm/index.mjs";

try {
    if (typeof TripettoAutoscrollAsDefaultImport.AutoscrollRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof TripettoAutoscroll.AutoscrollRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }

    if (typeof AutoscrollRunner === "undefined") {
        throw new Error("ES6 module failed!");
    }
} catch(e) {
    throw new Error("ES6 module failed!");
}
